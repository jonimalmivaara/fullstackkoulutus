// class Auto {
//     constructor (color, cylinders, year, velocity) {
//         this.color = color;
//         this.cylinders = cylinders;
//         this.year = year;
//         this.velocity = velocity;
//     }
    
//     brake () {
//         this.velocity = 0;
//     }
    
//     tulosta () {
//         console.log(`Auto on vuosimallia ${this.year}, se on väriltään ${this.color} ja siinä on ${this.cylinders} sylinteriä`);
//         console.log(`Auto kulkee ${this.velocity} km/h tunnissa`);
//     }

//     maalaa (color) {
//         this.color = color;
//     }
// }

// const peetunAuto = {
//     color: "harmaa",
//     cylinders: 6,
//     year: 2010,
//     velocity: 100,
//     brake: function() {
//         this.velocity = 0;
//     },
//     tulosta: function() {
//         console.log(`Auto on vuosimallia ${this.year}, se on väriltään ${this.color} ja siinä on ${this.cylinders} sylinteriä`);
//         console.log(`Auto kulkee ${this.velocity} km/h tunnissa`);
//     },
//     maalaa: function(color) {
//         this.color = color;
//     }
// };

// const pentinAuto = {
//     color: "punainen",
//     cylinders: 10,
//     year: 2020,
//     velocity: 60,
//     brake: function() {
//         this.velocity = 0;
//     },
//     tulosta: function() {
//         console.log(`Auto on vuosimallia ${this.year}, se on väriltään ${this.color} ja siinä on ${this.cylinders} sylinteriä`);
//         console.log(`Auto kulkee ${this.velocity} km/h tunnissa`);
//     },
//     maalaa: function(color) {
//         this.color = color;
//     }
// };

// peetunAuto.brake();
// peetunAuto.maalaa("sininen");
// peetunAuto.tulosta();

