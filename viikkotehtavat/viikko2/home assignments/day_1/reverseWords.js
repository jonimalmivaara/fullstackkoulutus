const string = process.argv[2];

const wordsArray = string.split(" ");

const reversedWords = wordsArray.map((word) => {
    const splitWords = word.split("");
    const reverseWord = splitWords.reverse().join("");
    return reverseWord;
});

console.log(reversedWords.join(""));