import axios from "axios";
import swal from "sweetalert";
import { Button } from "react-bootstrap";
import { useContext } from "react";
import { ProductContext } from "./Products";

function DeleteItem() {
  const context = useContext(ProductContext);

  const del = (id) => {
    axios.delete(`https://fakestoreapi.com/products/` + id).then((res) => {
      console.log("DELETE_PRODUCT", res.data);
      context.dispatch({
        type: "DELETE_PRODUCT",
        payload: res.data,
      });
      context.setShow(false);
    });
  };

  const deleteItem = (id) => {
    swal({
      title: "Are you sure?",
      text: "Item will be deleted permanently!",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal("Your item has been deleted!", {
          icon: "success",
          content: del(id),
        });
      } else {
        swal("Canceled!");
      }
    });
  };

  return (
    <Button variant="danger" onClick={() => deleteItem(context.item.id)}>
      Delete
    </Button>
  );
}

export default DeleteItem;
