const start = Number(process.argv[2]);
const end = Number(process.argv[3]);

let arr = [];

function range(start, end) {
    if(start < end) {
        for(let i = start; i <= end; i++) {
            arr.push(i);
        }
    } else {
        for(let i = start; i >= end; i--) {
            arr.push(i);
        }
    }
    console.log(arr);
}

range(start, end);