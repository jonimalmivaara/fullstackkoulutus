function Header() {
  return (
    <div>
      <header className="bg-dark py-2">
        <div className="container px-4 px-lg-5 my-5">
          <div className="text-center text-white">
            <h1 className="display-4 fw-bolder">FakeShop</h1>
            <p className="lead fw-normal text-white-50 mb-0">
              Can't really buy nothing. Saves money
            </p>
          </div>
        </div>
      </header>
    </div>
  );
}

export default Header;
