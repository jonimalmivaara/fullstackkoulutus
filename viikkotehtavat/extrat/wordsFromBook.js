import fs from "fs";

fs.readFile("./book.txt", "utf-8", (err, txt) => {
    if (err) console.log(err);
    else {
        const textArray = txt.split(/\s+|"/).filter(element => element.length > 3);
        console.log(textArray);
        let occurances = {};

        for (let word of textArray) {
            if(occurances[word]) {
                occurances[word]++;
            } else {
                occurances[word] = 1;
            }
        }
        
        const wordListArray = [];

        for(const [key, value] of Object.entries(occurances)) {
            wordListArray.push({word: key, occurance: value});
        }
        
        const topList = wordListArray.sort((a, b) => b.occurance - a.occurance);

        console.log(`
            Most common words longer than three letters
            -------------------------------------------
            Most occurances: "${topList[0].word}", ${topList[0].occurance} kpl.
            Second: "${topList[1].word}", ${topList[1].occurance} kpl.
            Third: "${topList[2].word}", ${topList[2].occurance} kpl.
            Fourth: "${topList[3].word}", ${topList[3].occurance} kpl.
            Fifth: "${topList[4].word}", ${topList[4].occurance} kpl.
            -------------------------------------------
        `);
        
    }
});