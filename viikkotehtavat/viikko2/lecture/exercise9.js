// this.grades = [
//     {subject: "Math", grade: 8.75},
//     {subject: "Finnish", grade: 6},
//     {subject: "History", grade: 7}
// ];

function Student(className, mathGrade, finnishGrade, historyGrade) {
    this.className = className;
    this.mathGrade = mathGrade;
    this.finnishGrade = finnishGrade;
    this.historyGrade = historyGrade;
}

Student.prototype.calculateAvarage = function() {
    return (this.mathGrade + this.finnishGrade + this.historyGrade) / 3;
};

Student.prototype.finnishYear = function() {
    const year = parseInt(this.className.substring(-1));
    if(year > 9) {
        return this.className = "graduated";
    } else {
        return this.className;
    }
};

// class Student {
    
//     constructor(className, mathGrade, finnishGrade, historyGrade) {
//         this.className = className;
//         this.mathGrade = mathGrade;
//         this.finnishGrade = finnishGrade;
//         this.historyGrade = historyGrade;
//     }

//     calculateAverage () {
//         const sum = this.mathGrade + this.finnishGrade + this.historyGrade;
//         const average = sum / 3;
//         return average;
//     }

//     finishYear () {
//         const year = parseInt(this.className[0]);
//         if(year < 9) {
//             const nextYear = year + 1;
//             const newClass = nextYear + this.className[1];
//             this.className = newClass;
//         } else {
//             this.className = "graduated";
//         }
//     }
// }


const joni = new Student("9B", 9, 8, 6);
const pasi = new Student("10A", 7, 4, 6);

console.log(`Avarage number is: ${joni.calculateAvarage()}`);
console.log(joni.finnishYear());
console.log(`Avarage number is: ${pasi.calculateAvarage()}`);
console.log(pasi.finnishYear());