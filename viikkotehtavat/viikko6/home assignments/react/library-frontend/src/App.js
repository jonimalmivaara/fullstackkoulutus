import './App.css';
import Login from './components/Login';
import Register from './components/Register';
import Library from './components/Library'
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import OwnerProvider from "./components/Provider";

export const BASE_URL= "http://localhost:5000/books/"

function Banner() {
  return (
    <div className='banner'>
      <h2 className='logo'>LIBRARY</h2>
    </div>
  )
}

function Home() {
  return (
    <div>
      <Library />
    </div>
  )
}

function App() {
  return (
    <OwnerProvider>
      <Router>
      <Banner />
        <Routes>
          <Route path="/" element={<Login/>}/>
          <Route path="/register" element={<Register/>}/>
          <Route path="/home" element={<Home/>} />
        </Routes>
      </Router>
    </OwnerProvider>
  );
}

export default App;
