import mongoose from "mongoose";

const ownerSchema = new mongoose.Schema({
    owner_firstname: String,
    owner_surname: String,
    contact_email: String,
    owner_address: String,
    owner_state: String,
    cars: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "cars"
    }]
});

const Owner = mongoose.model("owners", ownerSchema);

export default Owner;