// Bucketfill example

// const stack = [];
// while(stack.length > 0) {
//     //tällä voi korvata 3 riviä: const {x, y} = stack.pop();
//     const element = stack.pop();
//     const x = element.x;
//     const y = element.y;
//     canvas.setPixel(x, y).color = "red";
    
//     if(canvas.getPixel(x - 1, y).color === "white") {
//         stack.push({x: x - 1, y: y});
//     }
    
//     if(canvas.getPixel(x, y - 1).color === "white") {
//         stack.push({x: x, y: y - 1});
//     }
    
//     if(canvas.getPixel(x + 1, y).color === "white") {
//         stack.push({x: x + 1, y: y});
//     }
    
//     if(canvas.getPixel(x, y + 1).color === "white") {
//         stack.push({x: x, y: y + 1});
//     }
// } 

/*
************Arrays methods***************
*/

const instruments = ["Accordion", "Piano", "Synth", "Nyckelharpa"];

// instruments.forEach(n => console.log(n));

// const instrumentsLengths = instruments.map(str => str.length); //map

// console.log(instrumentsLengths);

// const instrumentsLengths = instruments.filter(str => str.length > 6); // filter

// const instrumentsLetter = instruments.filter(str => str.includes("a")); // filter

// const foundInstrunment = instruments.find(instruments => instruments[0] === "P"); // find

// const numbers = [1,2,4,7,3];

// const sumOfAllNumbers = numbers.reduce((acc, n) => acc += n);

const stringsTogether = instruments.reduce((acc, n) => acc + n, "alku ");

console.log(stringsTogether);

// console.log(sumOfAllNumbers, stringsTogether);

// const instruments = ["Accordion", "Piano", "Synth", "Nyckelharpa"];
// const lengths = instruments.map((instr) => instr.length);
// const longInstruments = instruments.filter((instr) => instr.length > 6);
// const piano = instruments.find((instrument) => instrument === "piano");
// const sum = lengths.reduce((accumulator, element) => accumulator + element, 0);