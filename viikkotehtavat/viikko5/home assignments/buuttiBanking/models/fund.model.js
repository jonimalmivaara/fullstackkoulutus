import mongoose from "mongoose";

const FundSchema = new mongoose.Schema({
    request_id: String,
    target_id: String,
    amount: Number,
    date: Date
});

const Fund = mongoose.model("Funds", FundSchema);
export default Fund;