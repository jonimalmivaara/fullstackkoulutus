import { Modal, Button } from "react-bootstrap";
import DeleteItem from "./DeleteItem";
import EditProductModal from "./EditProduct";
import { useContext } from "react";
import { ProductContext } from "./Products";

function ProductModal() {
  const context = useContext(ProductContext);

  return (
    <div className="modal">
      <Modal
        show={context.show}
        onHide={() => context.setShow(false)}
        animation={false}
      >
        <Modal.Header closeButton>
          <h5>{context.item.title}</h5>
        </Modal.Header>
        <Modal.Body>
          <img src={context.item.image} className="p-3 w-50" alt="" />
          <p>{context.item.description}</p>
          <h5>{context.item.price}€</h5>
        </Modal.Body>
        <Modal.Footer>
          <DeleteItem />
          <Button variant="secondary" onClick={() => context.setShow(false)}>
            Close
          </Button>
          <EditProductModal onClick={() => context.setShow(false)} />
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default ProductModal;
