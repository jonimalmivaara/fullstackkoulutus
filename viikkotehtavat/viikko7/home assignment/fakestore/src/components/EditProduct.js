import { Modal, Button, Form, NavDropdown, Nav } from "react-bootstrap";
import { useContext, useState } from "react";
import { ProductContext } from "./Products";
import axios from "axios";

function EditProductModal() {
  const context = useContext(ProductContext);

  const [show, setShow] = useState(false);
  const [title, setTitle] = useState(context.item.title);
  const [description, setDescription] = useState(context.item.description);
  const [price, setPrice] = useState(context.item.price);
  const [image, setImage] = useState(context.item.image);

  const submitHandler = (e) => {
    e.preventDefault();

    const data = {
      category: "",
      title: title,
      description: description,
      image: image
        ? image
        : "https://mbfn.org/wp-content/uploads/2020/09/image-coming-soon-placeholder.png",
      price: price ? price : 9999.99,
      rating: { rate: 0, count: 0 },
    };

    axios
      .put(`https://fakestoreapi.com/products/${context.item.id}`, data)
      .then((res) => {
        console.log("RESULT_DATA: ", res);
        if (res.status === 200) {
          context.dispatch({
            type: "EDIT_PRODUCT",
            payload: res.data,
          });
        } else {
          console.log("ERROR_UPDATING");
        }
      });
    setShow(false);
    context.setShow(false); // for lower level modal
  };

  return (
    <div>
      <Button variant="warning" onClick={() => setShow(true)}>
        Edit
      </Button>
      <div className="modal">
        <Modal show={show} onHide={() => setShow(false)} animation={false}>
          <Modal.Header
            closeButton
            variant="secondary"
            onClick={() => setShow(false)}
          >
            <h5>Edit product</h5>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={submitHandler}>
              <Form.Group className="mb-3" controlId="Category">
                <Form.Label>Category</Form.Label>
                <Nav className="me-auto">
                  <NavDropdown title="Category" id="basic-nav-dropdown">
                    {context.item.category}
                  </NavDropdown>
                </Nav>
              </Form.Group>
              <Form.Group className="mb-3" controlId="Title">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="Description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={5}
                  type="text"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="Price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="text"
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="Image">
                <Form.Label>Image source</Form.Label>
                <Form.Control
                  type="text"
                  value={image}
                  onChange={(e) => setImage(e.target.value)}
                />
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit changes
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    </div>
  );
}

export default EditProductModal;
