import express from "express";

const app = express();

app.get("/:name/:surname", (req, res) => {
    console.log("Params:");
    console.log(req.params);
    console.log("Query:");
    console.log(req.query);
    res.send("Hello " + req.params.name);
});

app.listen(8080);
   