// Return the number (count) of vowels in the given string. Let's consider a, e, i, o, u, y as vowels in this exercise.

function getVowelCount(str) {
    const vowels = "aeiouy";
    return str.split("").reduce((count, vowel) => vowels.includes(vowel) ? count += 1 : count, 0);
}

console.log(getVowelCount("abracadabra"));