import express from "express";
import { readJsonFile } from "./controllers/library.js";
import libraryRouter from "./routes/library.js";
import cors from "cors";

const app = express();
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use("/", libraryRouter);

readJsonFile();

app.listen(5000, () => {
    console.log("Listening port 5000");
});
