const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

const resultString = competitors.map((competitor, i) => {
    const place =  i < 3 ? ordinals[i] : ordinals[3];
    const results = `${(i + 1)}${place} competitor was ${competitor}`;
    return results;
});

console.log(resultString);