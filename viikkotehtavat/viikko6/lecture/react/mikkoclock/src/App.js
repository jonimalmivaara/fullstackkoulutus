import React, { useEffect, useState } from "react";
import './App.css';

function Clock() {
  const [time, setTime] = useState(new Date());

  const updateClock = () => {
    setTime(new Date());
  }
  
  useEffect(() => {
    const timer = setInterval(updateClock, 1000);
    return function cleanup() {
      clearInterval(timer);
    };
  }, []);

  return (
    <div className="time">
      <h1>{time.toLocaleTimeString()}</h1>
      <h3>Day {time.getDate()}</h3>
      <h3>Year {time.getFullYear()}</h3>
    </div>
  );
}

function Banner() {
  return <h1>Time now</h1>
}

function App() {
  return (
    <div className="App">
      <Banner />
      <Clock />
    </div>
)
}

export default App;
