// Exercise 2: countdown to start
// (Promises)
// Create a countdown program using Promises, with the
// setTimeout function.
// The program should output something like this.
// 3 ⇒ Wait 1 second
// ..2 ⇒ Wait another second
// ….1 ⇒ Wait the last 1 second..
// GO!

function print(txt) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log(txt);
            resolve();
        }, 1000);
    });
}

print("3")
    .then(() => print("..2"))
    .then(() => print("....1"))
    .then(() => print("GO!!!!!!"));

// function countDown(n) {
//     return new Promise((resolve) => {
  
//         console.log(n--);

//         if (n > 0) {
//             setTimeout( () => {
//                 countDown(n)
//                     .then(resolve);
//             }, 1000);
//         } else {
//             resolve("GO!!!!");
//         }

//     });

// }
  
// countDown(3)
//     .then((msg) => {
//         console.log(msg);
//     });

            // print(3)
            // .then(() => print(2))
            // .then(() => print(1))
            // .then(() => print("GO"))
            // .catch((err) => console.log(err));