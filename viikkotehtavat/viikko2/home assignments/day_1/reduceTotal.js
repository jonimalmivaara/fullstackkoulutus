function total(arr) {
    let sum = arr.reduce((acc , current) => acc + current);
    return sum;
}

console.log(total([1,2,3])); // 6