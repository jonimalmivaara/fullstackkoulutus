function myFilter(arr, fn) {
    const newArr = [];
    for(let i = 0; i < arr.length; i++) {
        const elem = arr[i];
        if(fn(elem)) {
            newArr.push(elem);
        }
    }
    return newArr;
}

const arr = [1,2,3,4,5,6,7,8];

const newarr = myFilter(arr, n => n < 4);
console.log(newarr);



function myMap(arr, fn) {
    const newArr = [];
    for(let i = 0; i < arr.length; i++) {
        const elem = arr[i];
        newArr.push(fn(elem));
    }
    return newArr;
}

function isEven(n) {
    return n % 2 === 0;
}

console.log([1,2,3,4,5,6,7,8].map(isEven));