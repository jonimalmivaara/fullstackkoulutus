import axios from "axios";
import { BASE_URL } from "../App";
import swal from 'sweetalert';

function DeleteItem(props) {

    const del = (id) => {
        axios
        .delete(BASE_URL + id, { headers: { Authorization: `bearer ${props.owner.token}` } })
        .then((res) => {
            const newItems = props.items.filter(item => item.id !== res.data.id);
            props.setItems(newItems);
            props.setLoading(true);
        })
    }

   const deleteItem = (id) => { 
        swal({
            title: "Are you sure?",
            text: "Item will be deleted permanently!",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
            swal("Your item has been deleted!", {
                icon: "success",
                content: del(id)
            });
            } else {
            swal("Canceled!");
            }
      });
    }

    return (
        <button className="btnDelete" onClick={() => deleteItem(props.id)}>x</button>
    )
}

export default DeleteItem;