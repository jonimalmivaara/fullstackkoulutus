import User from "../models/user.model.js";
import bcrypt from "bcrypt"; 

export async function checkPassword(user) {
    const foundUser = await User.findById(user.id);
    const match = bcrypt.compare(user.password, foundUser.password);
    return match;
}

export async function changeBalance(req, res) {
    const user = await User.findById(req.id);
    const currentBlance = user.balance;
    const newBalance = currentBlance + req.amount; // depends if request is minus (-100) its withdraw else its deposit

    if (newBalance < 0) { 
        res.status(400).send("Not enough money");
    } else {
        await User.findByIdAndUpdate({ _id: req.id }, { balance: newBalance });
        return newBalance;
    }
}
