import axios from "axios";
import React, { useState, useEffect } from "react";
import { Navbar, Container, NavDropdown, Nav } from "react-bootstrap";

function Category({ setLoadCategory }) {
  const [categories, setCategories] = useState("");

  useEffect(() => {
    axios.get("https://fakestoreapi.com/products/categories").then((res) => {
      setCategories(res.data);
    });
  }, []);

  if (!categories) {
    return null;
  } else {
    return (
      <div>
        <Navbar bg="light" expand="lg">
          <Container>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <NavDropdown title="Category" id="basic-nav-dropdown">
                  {categories.map((category, index) => (
                    <NavDropdown.Item
                      key={index}
                      onClick={() => setLoadCategory("category/" + category)}
                    >
                      {category}
                    </NavDropdown.Item>
                  ))}
                  <NavDropdown.Divider />
                  <NavDropdown.Item
                    href="#action/3.4"
                    onClick={() => setLoadCategory("")}
                  >
                    All
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}

export default Category;
