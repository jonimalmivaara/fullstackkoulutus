if(process.argv.length > 2) {
    const squareArea = parseFloat(process.argv[2]);
    if(isNaN(squareArea)) {
        console.log("error");
    } else {
        console.log(squareArea * squareArea);
    }
}