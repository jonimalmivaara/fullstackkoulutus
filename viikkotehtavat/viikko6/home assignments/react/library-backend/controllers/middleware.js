import jwt from "jsonwebtoken";

export const getAuth = (req, res, next) => {
    const auth = req.get("authorization");

    if (auth && auth.toLowerCase().startsWith("bearer ")) {
        const token = auth.substring(7);
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET);

        if (!token || !decodedToken.username) {
            return res.status(401).json({error: "token missing or invalid"});
        }

        next();
    }
};

export const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: "No one here!"});
};

export const errorHandler = (error, req, res, next) => {
    // Some error handling here!
};