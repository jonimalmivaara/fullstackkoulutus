import './App.css';
import { useState } from "react";

function Todo() {
  const [todoText, setTodoText] = useState();
  const [todos, setTodos] = useState([]);

  const handleSubmit = (event) => {

    if (todoText) {
      event.preventDefault();
      setTodos([...todos, {id: Math.random(), todo: todoText}])
      setTodoText("");

    } else {
      event.preventDefault();
      alert("Empty field!")
    }
  }

  const removeTodo = (id) => {
    const newTodos = todos.filter(todo => todo.id !== id);
    setTodos(newTodos);
  }

  return (
    <div className="todos">
      <form onSubmit={handleSubmit}>
        <input value={todoText} onChange={event => setTodoText(event.target.value)} type="text" placeholder="Add new Todo"></input>
        <input type="submit" value="Add"></input>
      </form>
      <div className="list">
        <ul>
          {todos.map(todo => (
            <li key={todo.id}>
              <input className="checkbox" type="checkbox"></input>
              <label>{todo.todo+" "}</label>
              <span onClick={() => removeTodo(todo.id)}> x </span>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

function Banner() {
  return (
    <div className="banner">
      <h1>TODO</h1>
    </div>
  )
} 

function App() {
  return (
    <div >
      <Banner />
      <Todo />
    </div>
  );
}

export default App;
