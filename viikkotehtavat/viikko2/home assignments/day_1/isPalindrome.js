const string = process.argv[2];

const sentence = string;
const reversedSentence = sentence.split("").reverse().join("");

console.log(reversedSentence);

if(sentence === reversedSentence) {
    console.log(`Yes! ${sentence} is palindrome`);
} else {
    console.log(`No, ${sentence} is  not palindrome`);
}