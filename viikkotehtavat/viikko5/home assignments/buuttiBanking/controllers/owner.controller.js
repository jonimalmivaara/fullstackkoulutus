import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import User from "../models/user.model.js";
import Fund from "../models/fund.model.js";
import bcrypt from "bcrypt";

dotenv.config();

const SALTROUNDS = 10;

export const getAuthedUserId = (req, res) => {
    const auth = req.get("authorization");

    if (auth && auth.toLowerCase().startsWith("bearer ")) {
        const token = auth.substring(7);
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET);

        if (!token || !decodedToken.id) {
            return res.status(401).json({error: "token missing or invalid"});
        }
        return decodedToken.id;
    }
    
};

export const postUser = async (req, res) => {
    const user = req.body;

    if (!user.name || !user.initial_deposit || !user.password) {
        return res.status(400).send("Invalid data");
    }

    const password = await bcrypt.hash(user.password, SALTROUNDS);
    const newUser = new User(
        {
            name: user.name,
            password: password,
            balance: user.initial_deposit
        });

    newUser.save()
        .then(() => {
            res.status(200).json({user_id: newUser.id});
        })
        .catch(err => {
            res.status(500).send(err);
        });
    
};

export const loginUser = async (req, res) => {
    const user = await User.findOne({name: req.body.username});

    // Get user and check username and password
    // Compare eneterd password with stored hash
    try {
        await bcrypt.compare(req.body.password, user.password)
            .then((isCorrectPassword) => {
                if(isCorrectPassword) {
                    const token = jwt.sign(
                        { id: user.id },
                        process.env.JWT_SECRET,
                        { expiresIn: "1h" }
                    );
                    res.status(200).send({token});
                } else {
                    res.status(401).send("Wrong password");
                }
            })
            .catch(err => console.log(err));

    } catch {
        console.log("Wrong username or password!");
    }
};

export const patchUsername = async (req, res) => {
    try {
        const id = req.body.id;
        const new_name = req.body.new_name;
    
        await User.findOneAndUpdate({_id: id}, {name: new_name});
        return res.status(200).json({new_username: new_name});
    } catch (error) {
        res.status(500).send("Invalid request");
    }
};

export const patchPassword = async (req, res) => {
    try {
        const id = req.body.id; 
        const newPassword = req.body.new_password;
        const password_hash = await bcrypt.hash(newPassword, SALTROUNDS);

        await User.findOneAndUpdate({_id: id}, {password: password_hash});
        return res.status(200).json({new_password: password_hash});
    } catch (error) {
        res.status(500).send("Invalid request");
    }
};

export const postFundRequest = async (req, res) => {
    try {
        const id = req.body.target_id;
        const request_id = getAuthedUserId(req);
        const amount = req.body.amount;
        const targetUser = await User.findById(id);

        const newFundRequest = new Fund(
            {
                request_id: request_id,
                target_id: id,
                date: new Date,
                amount: amount
            });
    
        const savedFund = await newFundRequest.save();
        targetUser.requests = targetUser.requests.concat(savedFund);
        await targetUser.save();
        res.status(200).json(savedFund);

    } catch (error) {
        res.status(500).send("Invalid request");
    }
};

export const getRequests = async (req, res) => {
    const user = await User
        .findById(req.params.user_id)
        .populate("requests");
  
    res.json({requests: user.requests});
};

export const deleteRequest = (req, res) => {
    Fund.findByIdAndRemove({_id: req.params.fund_id})
        .then(deleteFund => {
            res.json(deleteFund);
        })
        .catch(error => res.send(error));
};