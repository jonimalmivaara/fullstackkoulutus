"use strict";

const form = document.getElementById("login");

form.addEventListener("submit", (e) => {
    e.preventDefault();

    const username = form.elements.username;
    const password = form.elements.password;

    console.log(username.value);
    console.log(password.value);
});

const button = document.querySelector(".btn-red");
const button2 = document.querySelector(".btn-blue");

// button.style["background-color"] = "brown";

button.addEventListener("click", () => {
    button.classList.toggle("btn-red");
});

button2.addEventListener("click", () => {
    button2.classList.toggle("btn-blue");
});

