import React, { useState, useContext, useReducer } from "react";

const languageContext = React.createContext("fi");

const initialState = { name: "", nation: "", address: "", mainStudy: "" };

const selectedLanguage = {
  title: {fi: "Opiskelijan tiedot", en: "Student information"},
  name: {fi: "Nimi", en: "Name"},
  nation: {fi: "Kansalaisuus", en: "Nationality"},
  address: {fi: "Osoite", en: "Address"},
  mainStudy: {fi: "Pääaine", en: "Field of Study"},
  submit: {fi: "Tallenna", en: "Save"},
  lang: {fi: "In English", en: "Suomeksi"}
};

function reducer(information, action) {
    switch (action.type) {
      case "setData":
      return { name: action.data.name, nation: action.data.nation, address: action.data.address, mainStudy: action.data.mainStudy };
    default:
    console.log("error");
  }
}

function App() {
  const [language, setLanguage] = useState("fi");
  const [information, dispatch] = useReducer(reducer, initialState);

  const handleSubmit = (e) => {
    e.preventDefault()
    
    dispatch({
      type: "setData",
      data: {
        name: e.target.name.value,
        nation: e.target.nation.value,
        address: e.target.address.value,
        mainStudy: e.target.mainStudy.value,
      }
    })
  }
  
  const Form = () => {
    const lang = useContext(languageContext);

    return (
      <div className="row mb-5">
        <form onSubmit={handleSubmit}>
          <label className="col-1">{selectedLanguage.name[lang]}</label>
          <input className="col-3" id="name"></input><br/>
          <label className="col-1">{selectedLanguage.nation[lang]}</label>
          <input className="col-3" id="nation"></input><br/>
          <label className="col-1">{selectedLanguage.address[lang]}</label>
          <input className="col-3" id="address"></input><br/>
          <label className="col-1">{selectedLanguage.mainStudy[lang]}</label>
          <input className="col-3" id="mainStudy"></input><br/>
          <input type="submit" className="btn btn-success" value={selectedLanguage.submit[lang]}></input>
        </form>
      </div>
    )
  }

  const ShowStudent = () => {

    const lang = useContext(languageContext);

    return (
      <div >
        <h3>{selectedLanguage.title[lang]}</h3>
        <Form />
        <p><strong>{selectedLanguage.name[lang]}</strong> : {information.name}</p>
        <p><strong>{selectedLanguage.nation[lang]}</strong> : {information.nation}</p>
        <p><strong>{selectedLanguage.address[lang]}</strong> : {information.address}</p>
        <p><strong>{selectedLanguage.mainStudy[lang]}</strong> : {information.mainStudy}</p>
        <button className="btn btn-success" onClick={() => setLanguage((language === "fi") ? "en" : "fi")}>{selectedLanguage.lang[lang]}</button>
      </div>
    )
  }

  return (
    <languageContext.Provider value={language}>
      <ShowStudent />
    </languageContext.Provider>
  );
}

export default App;
