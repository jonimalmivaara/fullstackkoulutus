const a = process.argv[2];
const b = process.argv[3];
const c = process.argv[4];

const array = [a,b,c];
const sorted = array.sort(function(a, b) {
    return b.length - a.length;
});

if(!a || !b || !c) {
    console.log("Give three names (example: node lengthcomparison.js Maria Joe Philippa -> Philippa Maria Joe)");
} else {
    console.log(sorted.join(" "));
}