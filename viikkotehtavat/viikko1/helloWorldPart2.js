const lang = process.argv[2];

// if

if (lang === "fi") {
    console.log("Halloota muuailma");
} else if (lang === "en") {
    console.log("Hello World");
} else if (lang === "es") {
    console.log("hablo pablo");
} else {
    console.log("Hello World");
}


// switch-case

switch(lang) {
case "fi":
    console.log("Halloota muuailma");
    break;
case "es":
    console.log("hablo pablo");
    break;
default:
    console.log("Hello World");
    break;
}

// array

const arr = [
    { "lang": "en", "message": "Hello world" },
    { "lang": "es", "message": "Hola mundo" },
    { "lang": "fi", "message": "Hei maailma" },
];

let printed = false;
for (let i = 0; i < arr.length; i++) {
    if (arr[i].lang === lang) {
        console.log(arr[i].message);
        printed = true;
        break;
    }
}

if (!printed)
    console.log("Hello world");