import express from "express";
import carsRouter from "./routes/cars.router.js";
import dotenv from "dotenv";
import mongoose from "mongoose";
import { readJsonFile, insertCarsAndOwners } from "./utils/jsonToMongo.js";

const json = process.argv[2];

console.log("You can add car from json to mongo with argument 'json'");

function jsonToMongo() {
    if(json === "json") { 
        readJsonFile();
        insertCarsAndOwners();
    }
}

dotenv.config();

async function connectMongoose() { 
    try {
        await mongoose.connect(
            "mongodb://localhost/usedCars",
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true,
                user: process.env.MONGO_USER,
                pass: process.env.MONGO_PWD
            }
        );
    } catch (err){
        console.log(err);
    }
}

connectMongoose();
jsonToMongo();

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/", carsRouter);

app.listen(5000, () => {
    console.log("Listening to 5000");
});