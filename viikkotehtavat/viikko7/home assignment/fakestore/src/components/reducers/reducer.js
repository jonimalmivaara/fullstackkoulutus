export const initialState = [
  {
    id: 0,
    title: "",
    price: 0,
    description: "",
    category: "",
    rating: { rate: 0, count: 0 },
  },
];

export const reducer = (products, action) => {
  console.log(action);
  switch (action.type) {
    case "SET_PRODUCTS":
      return action.payload;
    case "ADD_PRODUCT":
      return products.concat(action.payload);
    case "DELETE_PRODUCT":
      return (products = products.filter(
        (item) => item.id !== action.payload.id
      ));
    case "EDIT_PRODUCT":
      return (products = products.map((item) =>
        item.id === action.payload.id ? action.payload : item
      ));
    default:
      console.log("error");
  }
};
