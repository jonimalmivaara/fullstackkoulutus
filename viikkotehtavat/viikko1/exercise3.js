const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];

console.log(arr); // prints array

console.log(arr.join(" "));

console.log(arr.sort()); // alph. order

arr.shift(); // remove first

console.log(arr);

arr.push("sipuli"); // add sipuli

console.log(arr);


for(let i = 0; i < arr.length; i++) {
    console.log(arr[i].includes("r"));
}
