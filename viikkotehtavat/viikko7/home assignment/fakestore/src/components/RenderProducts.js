import { useContext } from "react";
import { ProductContext } from "./Products";

function RenderProducts() {
  const context = useContext(ProductContext);

  return (
    <div className="container px-4 px-lg-5 mt-5">
      <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
        {context.products.map((item) => (
          <div
            key={item.id}
            className="col mb-3"
            onClick={() => context.setItem(item)}
          >
            <div className="h-100 pt-3 product-div">
              <img
                className="card-img-top m-auto"
                style={{ width: "100px" }}
                src={item.image}
                alt={item.title}
                variant="primary"
                onClick={() => context.setShow(true)}
              ></img>
              <div className="card-body p-4">
                <div className="text-center">
                  <h5 className="fw-bolder">{item.title}</h5>
                  <p className="fs-6">{item.price}€</p>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default RenderProducts;
