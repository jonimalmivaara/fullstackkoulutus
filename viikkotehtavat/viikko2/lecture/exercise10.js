class Shape{
    constructor(width, height) {
        this.width = width;
        this.heigth = height;
    }

    area () {
        // override this
        return -1;
    }

    circumference() {
        // override this
        return -1;
    }
}

class Circle extends Shape{
    constructor(diameter) {
        super(diameter, diameter);
    }

    circumference() {
        return Math.PI * this.width;
    }

    area() {
        return 2 * Math.PI * (this.width / 2) ** 2;
    } 
}

class Reactangle extends Shape{
    constructor(width, height) {
        super(width, height);
    }

    circumference() {
        return this.heigth * 2 + this.width * 2;
    }

    area() {
        return this.heigth * this.width;
    }
}

class Triangle extends Shape{
    constructor(width, height) {
        super(width, height);
    }

    area() {
        return this.width  * this.heigth / 2;
    }

    circumference() {
        const hypotenuse = Math.sqrt((this.width/2)**2+this.heigth);
        return this.width + hypotenuse * 2;
    }
}


const shape = new Shape(5, 3);
const circle = new Circle(5);
const reactangle = new Reactangle(5, 3);
const triangle = new Triangle(2, 1);

const shapes = [shape, circle, reactangle, triangle];

for(const shape of shapes) {
    console.log("width " + shape.width);
    console.log("heigth " + shape.heigth);
    console.log("area " + shape.area());
    console.log("circumference " + shape.circumference());
    console.log("------------");
}

console.log(shape);
console.log(circle.area(), circle.circumference());
console.log(reactangle.area(), reactangle.circumference());
console.log(triangle.area(), triangle.circumference());