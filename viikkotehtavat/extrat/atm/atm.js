import * as fs from "fs";
import * as readline from "node:readline";
import cron from "./cronJob.js";

const rl = readline.createInterface({
    input: process.stdin, 
    output: process.stdout, 
});

const menu = ["Saldokysely","Nosto","Lahjoita hyväntekeväisyyteen","Käytön lopettaminen"];
const nosto = [20,50,100,200,500,"Muu summa"];
const lahjoitus = [5,10,20,50,"Muu summa"];

let balance = 3500;

function saveLog(event, sum) { 
    let data; 
    
    try {
        data = fs.readFileSync("log.json", "utf-8");
    } catch (error) { 
        data = "[]";
    }
    const logs = JSON.parse(data); 

    const newLog = {
        "event": event,
        "sum": sum,
        "timeStamp": Date.now()
    };

    logs.push(newLog);

    fs.writeFile("log.json", JSON.stringify(logs), (err) => {
        if(err) console.log(err); 
    });
}

function timerToMainMenu() {
    setTimeout(() => atm(), 2000);
}

function tiliTapahtumaProsessointi(txt, summa, time) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log(txt, summa+"€");
            resolve();
        }, time);
    });
}

async function calculate(summa, infoTeksti) {
    await tiliTapahtumaProsessointi("Saldo:", balance, 500);
    await tiliTapahtumaProsessointi("Nosto: -", parseInt(summa), 1000);
    await tiliTapahtumaProsessointi("Nostettavissa:", balance - summa, 1500);
    balance = balance - summa;
    saveLog(infoTeksti, summa);
    timerToMainMenu();
}

function checkBalance(nostoSumma, infoTeksti) {
    if(balance < nostoSumma) {
        console.log("Tilillä ei riittävästi katetta!!!");
        timerToMainMenu();
    } else {
        calculate(nostoSumma, infoTeksti);
    }
}

function annaSumma(infoTeksti) {
    rl.question("Anna summa: ", (answer) => {
        if(parseInt(answer) % 2 === 0 && parseInt(answer) % 10 === 0) {
            checkBalance(answer, infoTeksti);
        }else {
            console.log(`
            Nostettavat setelit ovat ainoastaan 10€, 20€ ja 50€ seteleitä.
            Et voi nostaa summaa ${answer}€
            `);
            annaSumma();
        }
    });
}

function tiliOtto(tapahtuma, infoTeksti) {
    console.table(tapahtuma);
    
    rl.question(`Valitse summa (indexinumerolla), tapahtuma: ${infoTeksti} saldo: ${balance}€: `, (answer) => {
        if(parseInt(answer) === tapahtuma.length - 1) {
            annaSumma(infoTeksti);
        } else if(answer > tapahtuma.length - 1) {
            console.log(`
            Syötäthän summan indexinumerona.
            Tapahtuma peruttu!
            `);
            return timerToMainMenu();
        } else {
            checkBalance(tapahtuma[answer], infoTeksti);
        }
    });
}

function saldokysely() {
    console.table(`
        Tilisi saldo on:
        ${balance}€
        `);
    saveLog("saldokysely", balance);
    return timerToMainMenu();
}

function atm() {
    console.table(menu);
    rl.question("Valitse numerolla? ", (answer) => {
        switch(answer) {
            case "0":
                saldokysely();
                break;
            case "1":
                tiliOtto(nosto, "nosto");
                break;
            case "2":
                tiliOtto(lahjoitus, "lahjoitus");
                break;
            case "3":
                console.log("Kiitos käynnistä");
                process.exit();
                break;
            default:
                console.log("Anna oikea tapahtuma indexinumerolla!");
                atm();
                break;
        }
    });
}

cron();
atm();