function stringConcat(arr) {
    return arr.reduce((acc, current) => acc + current, "");
}

console.log(stringConcat([1,2,3])); // "123"