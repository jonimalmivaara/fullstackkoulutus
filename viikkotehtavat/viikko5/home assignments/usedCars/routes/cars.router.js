import express from "express";
import * as carController from "../controllers/cars.controller.js";

const router = express.Router();

router.get("/", carController.getAllCars);
router.get("/userCar/:id", carController.getCar);
router.get("/owners/:id", carController.getOwner);
router.post("/owners", carController.postOwner);
router.post("/cars", carController.postCar);
router.put("/userCar/:id", carController.putCar);
router.delete("/userCar/:id", carController.deleteCar);

export default router;