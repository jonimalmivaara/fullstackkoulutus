const days = 365;
const hours = 24;
const seconds = hours * 60 * 60;
const secondsInYear = days * seconds;

console.log(`There are ${secondsInYear} seconds in a year`);