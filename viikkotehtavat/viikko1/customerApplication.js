const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
});

readline.question("Kuinka mones kävijä tänään? ", (answer) => {
    if (answer % 2000 === 0) {
        console.log("Saa lahjakortin!!!");
    } else if (answer >= 1000 && answer % 25 === 0) {
        console.log("Saa ilmapallon!");
    } else {
        console.log("Ei saa mitään. :(");
    }
    readline.close();
});