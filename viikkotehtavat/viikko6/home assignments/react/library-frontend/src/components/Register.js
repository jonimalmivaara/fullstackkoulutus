import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function Register() { 
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
  
    const onChangeHandler = (event) => {
      const {name, value} = event.currentTarget;
      if (name === 'name') setName(value);
      else if (name === 'password') setPassword(value);
    }
  
    const navigate = useNavigate()

    const registerNewUser = () => {
        const data = {
            username: name,
            password: password
        };

        axios
            .post("http://localhost:5000/register", data)
            .then(() => {
                navigate("/home")
            })
    }
    
    return (
      <div className="login-page" style={{paddingTop:"7em"}}>
        <p>REGISTER NEW USER</p>
        <form>
          <input 
            type='text' 
            placeholder="Username" 
            name="name" 
            onChange={(event) => onChangeHandler(event)}/>
          <input  
            type='password' 
            placeholder="Password" 
            name="password" 
            onChange={(event) => onChangeHandler(event)}/>
          <input  
            type='password' 
            placeholder="Re-type password" 
            name="password"/>
          <input type='button' value='Register' onClick={(event) => registerNewUser(event.target.value)}/>
        </form>
        <span style={{fontSize:"smaller", cursor:"pointer"}} onClick={() => navigate("/")}>Back to login</span>
      </div>
    )
  }

export default Register;