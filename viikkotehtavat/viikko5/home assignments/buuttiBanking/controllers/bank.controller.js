import User from "../models/user.model.js";
import { changeBalance, checkPassword } from "../utils/bank.utils.js";
import Fund from "../models/fund.model.js";

export const getUserBalance = async (req, res) => {
    try {
        const user = await User.findById(req.params.user_id);
        if (user) res.json({account_balance: user.balance});
        else res.status(404).end();

    } catch(error) {
        res.status(500).send("Invalid request");
    }
};

export const postTransfer = async (req, res) => {
    const user = req.body;
    user.amount = -user.amount; // - for user money
    const recipient = { id: req.body.recipient_id, password: req.body.password, amount: -req.body.amount }; // + for recipient
    const passwordCheck = await checkPassword(user);

    if (!passwordCheck) {
        return res.status(401).send("Wrong password!");
    }

    try {
        await Promise.all([changeBalance(user, res), changeBalance(recipient)])
            .then(response => {
                if (response[0]) res.status(200).json({new_account_balance: response[0]});
                else res.status(404).end();
            });
    } catch (error) {
        res.status(500).send("Invalid request");
    } 
};

export const patchWithdraw = async (req, res) => {
    try {
        const user = req.body;
        const passwordCheck = await checkPassword(user);

        if(!passwordCheck) {
            return res.status(401).send("Wrong password!");
        }

        const balance = await changeBalance(user, res);
        if (balance) res.status(200).json({new_account_balance: + balance});
        else res.status(404).end();
    } catch (error) {
        res.status(500).send("Invalid request");
    } 
};

export const patchDeposit = async (req, res) => {
    try {
        const user = req.body;
        const passwordCheck = await checkPassword(user);

        if(!passwordCheck) {
            return res.status(401).send("Wrong password!");
        }

        const balance = await changeBalance(user, res);
        if (balance) res.status(200).json({new_account_balance: + balance});
        else res.status(404).end();
    } catch (error) {
        res.status(500).send("Invalid request");
    } 
};

export const postAcceptFund = async (req, res) => {
    const fund = await Fund.findByIdAndRemove({_id: req.params.fund_id});

    const target_id = { id: fund.target_id, amount: -fund.amount };
    const request_id = { id: fund.request_id, amount: fund.amount };

    try {
        await User.findByIdAndUpdate({ _id: target_id }, { balance: -fund.amount });
        await User.findByIdAndUpdate({ _id: request_id }, { balance: fund.amount });

    } catch (error) {
        res.status(500).send("Invalid request");
    } 
};


