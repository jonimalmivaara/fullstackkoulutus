import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import bcrypt from "bcrypt";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";

dotenv.config();

export let currentUser;

export let users = [];

function updateJsonFile(file, itemsToUpdate) {
    fs.writeFileSync(file, JSON.stringify(itemsToUpdate), "utf-8");
}

function getUser(userRequest) {
    for(let i = 0; i< users.length; i++) {
        if(users[i].username === userRequest) {
            const user = users[i];
            return user;
        }
    }
}

export const postUserId = async (req, res) => {
    users = JSON.parse(fs.readFileSync("users.json", "utf-8"));

    // Get user and check username and password
    const user = await (getUser(req.body.username));
    // Compare eneterd password with stored hash
    try {
        bcrypt.compare(req.body.password, user.password)
            .then((isCorrectPassword) => {
                if(isCorrectPassword) {
                    const token = jwt.sign(
                        { username: req.body.username, id: user.owner_id },
                        process.env.JWT_SECRET
                    );
                    currentUser = user.owner_id;
                    res.status(200).send({token});
                } else {
                    res.status(401).send("Wrong password");
                }
            })
            .catch(err => console.log(err));
    } catch {
        console.log("Wrong username or password!");
    }
};

export const registerUserId = async (req, res) => {
    const saltRounds = 10;

    const username = req.body.username;
    const password = await bcrypt.hash(req.body.password, saltRounds);  

    const user = {
        "username":username,
        "password":password,
        "owner_id":uuidv4()
    };
    
    users = users.concat(user);
    updateJsonFile("users.json", users);

    res.send("User stored and password hashed for user: " + req.body.username);
    res.end();
};