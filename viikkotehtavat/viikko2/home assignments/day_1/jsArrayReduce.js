function countLetters(str) {
    const sumLetters = str.split("").reduce((acc, current) => {
        console.log(acc);
        if(current in acc) {
            acc[current] += 1;
        } else {
            acc[current] = 1;
        }
        return acc;
    }, {});
    return sumLetters;
}

console.log(countLetters("abbcccddddeeeee"));
