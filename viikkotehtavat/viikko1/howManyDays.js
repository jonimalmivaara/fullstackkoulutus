const month = parseInt(process.argv[2]);

if(!month || month < 1 || month > 12) {
    console.log("Give month number 1-12!");
} else if(month === 2) {
    console.log(`There are 28 days in month ${month}`);
} else if(month <= 6 && month % 2 === 0) {
    console.log(`There are 30 days in month ${month}`);
} else if(month < 7) {
    console.log(`There are 31 days in month ${month}`);
} else if(month > 7 && month % 2 === 0 || month === 7) {
    console.log(`There are 31 days in month ${month}`);
} else {
    console.log(`There are 30 days in month ${month}`);
}

// switch-case

// const month = Number(process.argv[2]);

// switch (month) {
// case 2:
//     console.log("28 days");
//     break;
// case 1:
// case 3:
// case 5:
// case 7:
// case 8:
// case 10:
// case 12:
//     console.log("31 days");
//     break;
// case 4:
// case 6:
// case 9:
// case 11:
//     console.log("30 days");
//     break;
// default:
//     console.log("no such month");
// }