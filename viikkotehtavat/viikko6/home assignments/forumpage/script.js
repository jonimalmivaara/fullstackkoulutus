"use strict";

const submit = document.getElementById("submitBtn");
const toggle = document.getElementById("toggleBtn");
const form = document.getElementById("form");
const formDiv = document.querySelector(".postForm");

submit.addEventListener("click", (e) => post(e));
toggle.addEventListener("click", () => toggleAddPost());

const toggleAddPost = () => {
    formDiv.classList.toggle("hidden");
};

// eslint-disable-next-line no-unused-vars
const deletePost = (id) => {
    document.getElementById(`post${id}`).remove();
};

const post = (e) => {
    e.preventDefault();
    const id = Math.random();
    const name = document.getElementById("name").value;
    const post = document.getElementById("post").value;

    if (!name || !post) {
        return alert("Name or post are empty!");
    }

    const div = `
        <div class="forumpost" id="post${id}">
        <h2>${name}</h2>
        <button onclick="deletePost(${id})">Delete</button>
        <p>${post}</p>
    `;

    document.querySelector(".messages").innerHTML += div;
    form.reset();
};