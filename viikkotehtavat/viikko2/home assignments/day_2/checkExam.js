// The first input array is the key to the correct answers to an exam, like ["a", "a", "b", "d"] . The second one contains a student's submitted answers.
// The two arrays are not empty and are the same length. Return the score for this array of answers, giving +4 for each correct answer, -1 for eachincorrect answer, and +0 for each blank answer, represented as an empty string.
// If the score < 0, return 0

// example checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"]) → 6

function checkExam(correctAnswer, studentAnswer) {
    const points = studentAnswer.reduce((points, answer, index) => {
        if(answer.length === 0) {
            points;
        } else {
            answer === correctAnswer[index] ? points += 4 : points--;
        }
        return points;
    },0);
    return points > 0 ? points: 0;
}

console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"])); //  6 
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b", ""])); // 7
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"])); // 16
console.log(checkExam(["b", "c", "b", "a"], ["", "a", "a", "c"])); // 0
