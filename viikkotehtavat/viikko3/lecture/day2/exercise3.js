const getValue = function () {
    return new Promise((res, rej) => {
        setTimeout(() => {
            res({ value: Math.random() });
        }, Math.random() * 1500);
    });
};

async function values() {
    const valueOne = (await getValue(3));
    const valueTwo = (await getValue(2));
    console.log(`Value 1 is ${valueOne.value} and value 2 is ${valueTwo.value}`);
}

values();

let valueOneHere;
getValue()
    .then(val => {
        valueOneHere = val.value;
        return getValue();
    })
    .then(val => {
        const valueTwoHere = val.value;
        console.log(`Value 1 is ${valueOneHere} and value 2 is ${valueTwoHere}`);
    });

Promise.all([getValue(), getValue()]) // Paras tapa
    .then(values => {
        console.log(`Value 1 is ${values[0].value} and value 2 is ${values[1].value}`);
    });