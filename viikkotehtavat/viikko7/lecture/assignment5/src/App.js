import './App.css';
import { useState } from "react";

function ChangeText() {
  const [text, setText] = useState("");
  const [showText, setShowText] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    setShowText(text)
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <h3>Your string is: {showText}</h3>
        <input
          type="text"
          value={text}
          onChange={e => setText(e.target.value)}>
        </input>
        <input type="submit" value="submit" />
      </form>
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <ChangeText />
    </div>
  );
}

export default App;
