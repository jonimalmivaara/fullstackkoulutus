import { useState } from "react";
import axios from "axios";
import { Modal } from "./Modal";
import { BASE_URL } from "../App";

function CreateItem(props) {
    const [isModalOpen, setIsModalOpen] = useState("modal hidden");
    const [title, setTitle] = useState();
    const [author, setAuthor] = useState("");
    const [image, setImage] = useState("");
  
    const handleSubmit = (event) => {
      // prevent normal submit event
      event.preventDefault();
  
      const data = {
        name: title,
        author: author,
        img: image,
        read: false
      };
  
        axios
          .post(BASE_URL, data, { headers: { Authorization: `bearer ${props.owner.token}` } })
          .then((res) => {
            const newItems = props.items.filter(item => item.id !== res.data.id);
            props.setItems(newItems);
            props.setLoading(true);
          });
    };

    return (
      <div>
        <Modal setIsModalOpen={setIsModalOpen}/>
        <div className={isModalOpen}>
            <button className="btnClose" onClick={() => setIsModalOpen("modal hidden")}>X</button>
            <div className='form' >
            {/* <img src={image} alt="img"/> */} 
            <form onSubmit={handleSubmit}>
              <input
                type="text"
                value={title}
                onChange={(event) => setTitle(event.target.value)}
                placeholder="Title"/>
              <input
                type="text"
                value={author}
                onChange={(event) => setAuthor(event.target.value)}
                placeholder="Author"/>  
              <input
                type="text"
                value={image}
                onChange={(event) => setImage(event.target.value)}
                placeholder="Image cover source"/><br/>
              <input type="submit" value="Add"/>
            </form>
          </div>
        </div>
      </div>
    )
  }

export default CreateItem;