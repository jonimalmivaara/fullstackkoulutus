import './App.css';
import { useState, useEffect } from "react";

function Timer() {
  const [isOn, setIsOn] = useState(false);
  const [seconds, setSeconds] = useState(0);
  const [minutes, setMinutes] = useState(0);

  useEffect(() => {
    let interval;

    if(isOn) { 
      interval = setInterval(() => setSeconds(seconds + 1), 1000);
    }

    if(seconds === 60) {
      setSeconds(0);
      setMinutes(minutes + 1);
    }
    
    return () => clearInterval(interval);
  }, [isOn, seconds, minutes])

  const reset = () => {
    setMinutes(0);
    setSeconds(0);
  }

  return (
    <div>
      <h1>Timer</h1>
      <h3>{minutes.toString().padStart(2, "0")}:{seconds.toString().padStart(2, "0")}</h3>
      <button onClick={() => setIsOn(!isOn)}>Start/Stop</button>
      <button onClick={() => setMinutes(minutes + 1)}>Add minute</button>
      <button onClick={() => setSeconds(seconds + 1)}>Add second</button>
      <button onClick={() => reset()}>Reset</button>
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <Timer />
    </div>
  );
}

export default App;
