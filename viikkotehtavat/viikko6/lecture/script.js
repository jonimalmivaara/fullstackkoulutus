let count = 1;

function load() {
    console.log("Hello world!");
    document.querySelector(".button").addEventListener("click", onClick);
}

function onClick() {
    alert(`You clicked ${count} times`);
    count++;
}

window.onload = load;