const fs = require("fs");

function getPassword(file) {
    return new Promise((resolve, reject) => {
        const txt = fs.readFileSync(file, "utf-8");

        if(txt === "S4l4s4n4!!") {
            resolve("Welcome user!");
        } else {
            reject("Wrong password!");
        }
    });
}

getPassword("test.txt")
.then(msg => {
    console.log(msg);
    // lataa salaiset dokumentit
    // -----
})
.catch(err => {
    console.log(err);
})