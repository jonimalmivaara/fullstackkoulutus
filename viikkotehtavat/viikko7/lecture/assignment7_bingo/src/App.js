import './App.css';
import { useState } from "react";

function Bingo() {
  const [numbers, setNumbers] = useState([]);

  const newNumber = () => {
    return Math.floor((Math.random() * 75) + 1)
  }

  const addNumber = () => {
    console.log({numbers});
    if (numbers.length === 75) {
      return alert("No more numbers left!");
    }

    const number = newNumber()

    if (!numbers.includes(number)) {
      setNumbers([...numbers, number]);
    } else {
      addNumber();
    }
  }

  const numberOrder = () => {
    const newNums = numbers.reverse();
    setNumbers([...newNums]);
  }

  return (
    <div>
      <div className="buttons">
        <button className="btn add" onClick={() => addNumber()}>Add number</button>
        <button className="btn order" onClick={numberOrder}>Order</button>
        <button className="btn reset" onClick={() => setNumbers([])}>Reset</button>
      </div>
      <div className="numbers">
        <ShowNumbers numbers={numbers} />
      </div>
    </div>
  )
}

function ShowNumbers(props) {
  return (
    props.numbers.map((number, index) => (
      <div key={index} className="balls">
        <span>{number}</span>
      </div>
    ))
  )
}

function Banner() {
  return (
  <div className="Banner">
    <h1>BINGO</h1>
  </div>
)}

function App(props) {
  return (
    <div className="App">
      <Banner />
      <Bingo />
    </div>
  );
}

export default App;
