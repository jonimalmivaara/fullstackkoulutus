import { Modal, Button, Form, NavDropdown, Nav } from "react-bootstrap";
import { useState } from "react";
import axios from "axios";
import { useContext } from "react";
import { ProductContext } from "./Products";

function AddProductModal() {
  const [show, setShow] = useState(false);
  const context = useContext(ProductContext);

  const submitHandler = (e) => {
    e.preventDefault();

    const data = {
      category: "",
      title: e.target.Title.value,
      description: e.target.Description.value,
      image: e.target.Image.value
        ? e.target.Image.value
        : "https://mbfn.org/wp-content/uploads/2020/09/image-coming-soon-placeholder.png",
      price: e.target.Price.value ? e.target.Price.value : 9999.99,
      rating: { rate: 0, count: 0 },
    };

    axios.post("https://fakestoreapi.com/products", data).then((res) => {
      console.log("RESULT_DATA: ", res);
      if (res.status === 200) {
        context.dispatch({
          type: "ADD_PRODUCT",
          payload: { ...res.data, id: Math.random() * 100 },
        });
      } else {
        console.log("ERROR_POSTING_DATA");
      }
    });
    setShow(false);
  };

  return (
    <div>
      <button
        className="btn btn-dark btn-pull-top-right"
        onClick={() => setShow(true)}
      >
        Add product
      </button>
      <div className="modal">
        <Modal show={show} onHide={() => setShow(false)} animation={false}>
          <Modal.Header
            closeButton
            variant="secondary"
            onClick={() => setShow(false)}
          >
            <h5>Add new product</h5>
          </Modal.Header>{" "}
          <Modal.Body>
            <Form onSubmit={submitHandler}>
              <Form.Group className="mb-3" controlId="Category">
                <Form.Label>Category</Form.Label>
                <Nav className="me-auto">
                  <NavDropdown title="Category" id="basic-nav-dropdown">
                    <Form.Control type="text" />
                  </NavDropdown>
                </Nav>
              </Form.Group>
              <Form.Group className="mb-3" controlId="Title">
                <Form.Label>Title</Form.Label>
                <Form.Control type="text" placeholder="Enter title" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="Description">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={5}
                  type="text"
                  placeholder="Description"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="Price">
                <Form.Label>Price</Form.Label>
                <Form.Control type="text" placeholder="Price" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="Image">
                <Form.Label>Image source</Form.Label>
                <Form.Control type="text" placeholder="Image source" />
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    </div>
  );
}

export default AddProductModal;
