import icon from "../icons/modify_icon.png"

export const Modal = (props) => {
    return (
      <div>
        <button className="btnModal" onClick={() => props.setIsModalOpen("modal")}>Add Item</button>
      </div>
    )
}

export const ModifyModal = (props) => {
  return (
    <div>
      <img className="modifyIcon" src={icon} alt="btn"  onClick={() => props.setIsModifyModalOpen("modifyModal")}/>
    </div>
  )
}