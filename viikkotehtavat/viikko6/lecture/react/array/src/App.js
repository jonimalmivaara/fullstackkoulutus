import "./App.css";

function PrintNames(props) {
  const names = props.names.map((name, index) => {
    if (index % 2 === 0) {
      return (
        <p key={index} style={{ fontWeight: "bold" }}>
          {name}
        </p>
      );
    } else {
      return (
        <p key={index} style={{ fontStyle: "italic" }}>
          {name}
        </p>
      );
    }
  });

  return names;
}

function App() {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];

  return (
    <div className="App">
      <PrintNames names={namelist} />
    </div>
  );
}

export default App;
