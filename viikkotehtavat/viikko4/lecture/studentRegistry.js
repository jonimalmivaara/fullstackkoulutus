import express from "express";
import { v4 as uuidv4 } from "uuid";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

let students = [];

app.get("/students", (req, res) => {
    res.send(students);
});

app.get("/students/:id", (req, res) => {
    const student = students.find(student => student.id === req.params.id);
    res.send(student);
});

app.post("/students", (req, res) => {
    const student = req.body;
    student.id = uuidv4();
    students = students.concat(req.body);
    res.redirect("/");
});


app.put("/students/:id", (req, res) => {
    const newStudent = {...req.body, id: req.params.id};
    /*
    const newStudent = req.body;
    newStudent.id = req.params.id;
    */
    students = students.map(student => 
        student.id === req.params.id ? newStudent : student
    );
    /*students = students.map(student => {
        if(student.id === req.params.id) {
            return req.body;
        } else {
            return student;
        }
    });*/
    res.redirect("/");
});


app.delete("/students/:id", (req, res) => {
    students = students.filter(student => student.id !== req.params.id);
    res.redirect("/");
});

app.listen(5000, () => {
    console.log("Listening to 5000");
});