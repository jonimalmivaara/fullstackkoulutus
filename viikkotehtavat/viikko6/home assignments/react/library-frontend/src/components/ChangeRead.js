import axios from "axios";
import { BASE_URL } from "../App";

function ChangeRead(props) {

    const changeValue = (event, id) => {
    axios
      .patch(BASE_URL + id, { read: event } , { headers: { Authorization: `bearer ${props.owner.token}` } })
      .then((res) => {
        const newItems = res.data.filter(item => item.owner_id === props.item.owner_id);
        props.setItems(newItems);
        props.setLoading(false);
      });
    }

    return (
        <label class="switch">
            <input type="checkbox" checked={props.item.read ? true : false} onChange={(event) => changeValue(event.target.checked, props.item.id)}/>
            <span className="slider round">read</span>
        </label>
    )
}

export default ChangeRead