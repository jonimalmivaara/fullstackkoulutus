class NotFacebookPost {
    
    constructor(poster, text) {
        this.poster = poster;
        this.text = text;
        const today = new Date();
        this.date = today.getDate() + "." + (today.getMonth()+1) + "." + today.getFullYear();
        this.likes = 0;
        this.comments = [];
        this.deleted = false;
    }

    edit (newText) {
        this.text = newText;
    }

    delete () {
        this.deleted = true;
    }

    like () {
        this.likes++;
    }

    comment (comment) {
        this.comments.push(comment);
    }

    printComments() {
        this.comments.forEach(comment => comment.print());
    }

    print () {
        console.log(`${this.poster} posted at ${this.date}: ${this.text}`);
        this.printComments();
    }
}

class NotFacebookComment extends NotFacebookPost {
    
    constructor(poster, text, positive, layer) {
        super(poster, text);
        this.positive = positive;
        this.layer = layer;
    }

    print () {
        let status;
        if(this.positive) {
            status = "positively";
        } else {
            status = "negatively";
        }
        console.log(`${"  ".repeat(this.layer)}${this.poster} commented ${status} at ${this.date}: ${this.text}`);
        this.printComments(this.layer);
    }
}

const peetunPostaus = new NotFacebookPost("Peetu", "Karkki on tosi pahaa!!! :(");
const pertsanKommentti = new NotFacebookComment("Pertsa", "Hyvääpäs!!!", false, 1);
peetunPostaus.comment(pertsanKommentti);
const pirkkoKommentti = new NotFacebookComment("Pirkko", "Niinpä", false, 2);
pertsanKommentti.comment(pirkkoKommentti);
const makenKommentti = new NotFacebookComment("Make", "Totta. Syön vain porkkanoita!", true, 1);
peetunPostaus.comment(makenKommentti);

peetunPostaus.print();