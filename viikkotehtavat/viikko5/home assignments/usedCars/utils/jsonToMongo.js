import fs from "fs";
import Car from "../models/cars.model.js";
import Owner from "../models/owner.model.js";

let cars = [];

export function readJsonFile() {
    try {
        cars = JSON.parse(fs.readFileSync("user_cars.json", "utf-8"));
    } catch {
        cars = [];
    }
}

export async function insertCarsAndOwners() {
    cars.forEach(async (car) => {
        console.log(car);
        const newCar = new Car({
            car_make: car.car_make, 
            car_model: car.car_model, 
            car_modelyear: car.car_modelyear, 
            price: car.price
        });
        const savedCar = await newCar.save();

        const newOwner = new Owner({
            owner_firstname: car.first_name, 
            owner_surname: car.owner_surname, 
            contact_email: car.contact_email,
            owner_address: car.owner_address, 
            owner_state: car.owner_state, 
            cars: savedCar.id
        });

        await newOwner.save();
    });
}
