const readline = require("readline");

const interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let dieType;

function getDieType() {
    const argumentError = "Please provide the die type. Example: D6";
    const defaultDieType = 6;

    if (process.argv.length < 3) {
        console.log(argumentError);
        return;
    }

    const argument = process.argv[2];
    if (argument[0] !== "D") {
        console.log(argumentError);
        return;
    }

    // " D 1 0 0 ""
    //   0 1 2 3

    const dieTypeString = argument.substring(1);
    dieType = parseInt(dieTypeString);

    if (isNaN(dieType)) {
        console.log("Invalid die type. Example: D6");
        dieType = undefined;
        return;
    }

    if (dieType < 3 || dieType > 100) {
        console.log(`Invalid die side count, needs to be between 3 and 100. Falling back to default of ${defaultDieType}.`);
        dieType = defaultDieType;
    }
}

function rollDie() {
    const dieRollResult = getRandomInt(dieType);
    if (dieRollResult === 1) {
        console.log("Ouch! The roll is " + dieRollResult);
    }
    else if (dieRollResult === dieType) {
        console.log(`Yippii!!!! The roll is ${dieRollResult}`);
    }
    else {
        console.log("The roll is " + dieRollResult);
    }

    console.log("Press <enter> for a dice roll, type 'q' + <enter> to exit.");
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max) + 1;
}

getDieType();
if (dieType !== undefined) {
    rollDie();

    interface.on("line", (line) => {
        if (line === "q") {
            process.exit(0);
        }

        rollDie();
    });
}