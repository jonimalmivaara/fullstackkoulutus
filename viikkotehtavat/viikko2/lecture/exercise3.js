// function factorial(n){
//     if(n === 0 || n === 1){
//         return 1;
//     }else{
//         return n * factorial(n-1);
//     }
// }
// const n = 4;
// const answer = factorial(n);
// console.log(answer);

// FOR-LOOP

// const n = 9;
// let result = 1;

// for(let i = 1; i <= n; i++) {
//     result *= i;
// }

// console.log(result);

function factorial(n){
    console.log(n);

    if(n === 1) {
        return n;
    }
    return n * factorial(n -1);
}

console.log(factorial(9));
