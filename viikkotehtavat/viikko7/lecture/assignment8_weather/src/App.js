import './App.css';
import { useState, useEffect } from "react";
import axios from "axios";

const API_KEY = process.env.REACT_APP_API_KEY

function App() {
  const [weather, setWeather] = useState();
  const [city, setCity] = useState("Jyväskylä");
  const [cityText, setCityText] = useState();

  useEffect(() => {
    axios
      .get(`http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${city}`)
      .then((res) => {
        setWeather(res.data)
      })
  }, [city])

  const handleSubmit = (e) => {
    e.preventDefault()
    setCity(cityText);
  }

  if (!weather) {
    return <div>Loading...</div>

  } else {
    return (
      <div className="App">
        <h3>Check the current weather</h3>
        <form onSubmit={handleSubmit}>
          <input type="text" onChange={e => setCityText(e.target.value)}></input>
          <input type="submit" value="Search"></input>
        </form>
        <p>Current weather in {weather.location.name}</p>
        <p>Temperature {weather.current.temp_c} celcius</p>
        <p>{weather.current.condition.text}, Humidity {weather.current.humidity} %</p>
        <p>Feels like {weather.current.feelslike_c} celcius</p>
      </div>
  );
  }
}

export default App;
