import express from "express";
import fs from "fs";

const app = express();

let counter = 0;

app.use((req ,res, next) => { // midleware testing call

    const time =  new Date().toLocaleString();
    const log = time + " " + req.path + " " + JSON.stringify(req.query);

    fs.appendFile("log.txt", log+("\r\n"), (err) => {
        next();
    });
}); 

app.get("/counter", (req, res) => {
    counter++;
    if(req.query.number) {
        counter = parseInt(req.query.number);
    }
    res.send(`<h1>${counter}<h1>`);
});

const counterWithName = {};
app.get("/counter/:name", (req, res) => {
    if(!counterWithName[req.params.name]) {
        counterWithName[req.params.name] = 0;
    }
    counterWithName[req.params.name]++;
    console.log(counterWithName);
    res.send(`<h1>${req.params.name} was here ${counterWithName[req.params.name]} times <h1>`);
});

app.listen(5000, () => {
    console.log("Listening port 5000");
});