const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
});

const userInput = process.argv[2];
const errorMsg = "The number of sides should be a number between 3 and 100 and must start with 'D', example ( D100 )";

if(process.argv.length < 3) {
    console.log(errorMsg);
    process.exit();
}

let dice = parseInt(userInput.substring(1));

function diceApp() {
    if(dice > 100 || isNaN(dice) || process.argv[2][0] !== "D") {
        console.log(errorMsg);
        process.exit(1);
    } else if (dice < 3) { // falls back to default (6) if given number i below 3
        console.log("Dice must be 3 at least, falling back to 6"); 
        dice = 6;
        rollDice();
    } else {
        rollDice();
    }
}

function rollDice() {
    let number = Math.floor(Math.random() * (dice)) + 1;
    readline.question("Press <enter> for a userInput roll, type 'q' + <enter> to exit: ", function (answer) {
        if (answer === "q") {
            console.log("Bye!");
            process.exit(1);
        } else if (number === 1) {
            console.log(`Sad! The roll is ${number}`);
        } else if (number === dice) {
            console.log(`Happy! The roll is ${number}`);
        } else {
            console.log(`The roll is ${number}`);
        }
        rollDice();
    });
    
}   

diceApp();