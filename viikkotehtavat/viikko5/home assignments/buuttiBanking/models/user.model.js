import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3
    },
    password: {type: String, required: true},
    balance: Number,
    requests: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Funds"
    }]
});

const User = mongoose.model("Users", UserSchema);
export default User;