import fs from "fs";

fs.readFile("joulu.txt", "utf-8", (err, text) => {
    if(err) {
        console.log(err);
    } else {
        const words = text.split(" ");
        const changeWords = words.map((word) => {
            if(word === "joulu") word = "kinkku";
            if(word === "Joulu") word = "Kinkku";
            if(word === "lapsilla") word = "poroilla";
            if(word === "Lapsilla") word = "Poroilla";
            console.log(word);
            return word;
        });

        fs.writeFile("newJoulu.txt", changeWords.join(" "), (err) => {
            if(err) {
                console.log(err);
            } else {
                console.log("writed");
            }
        }); 
    }
});
