import Car from "../models/cars.model.js";
import Owner from "../models/owner.model.js";

export const getAllCars = (req, res) => {
    // not yet implented!!
};

export const getCar = async (req, res) => {
    try {
        const car = await Car.findById(req.params.id);
        if (car) res.json(car);
        else res.status(404).end();
    } catch(error) {
        console.log(error);
    }
};

export const getOwner = async (req, res) => {
    const owners = await Owner
        .findById(req.params.id)
        .populate("cars");
  
    res.json(owners);
};

export const postCar = (req, res) => {
    const data = req.body;

    if(!data.car_make || !data.car_model) {
        res.status(400).send("Invalid data");
    } else {
        const car = new Car({
            car_make: data.car_make,
            car_model: data.car_model,
            car_modelyear: data.car_modelyear,
            price: data.price
        });

        car.save()
            .then(result => {
                console.log(result);
            })
            .catch(err => {
                console.log("Error", err);
            });

        res.status(200).send("Car added with id: "+car._id);
    }
};

export const postOwner = (req, res) => {
    const data = req.body;

    if(!data.owner_firstname || !data.owner_surname) {
        res.status(400).send("Invalid data");
    } else {
        const owner = new Owner({
            owner_firstname: data.owner_firstname,
            owner_surname: data.owner_surname,
            contact_email: data.contact_email,
            owner_address: data.owner_address,
            owner_state: data.owner_state,
            cars: []
        });

        owner.save()
            .then(result => {
                console.log(result);
            })
            .catch(err => {
                console.log("Error", err);
            });

        res.status(200).send("Owner added with id: "+owner.id);
    }
};

export const putCar = async (req, res) => {
// under construction !!!
};

export const deleteCar = (req, res) => {
    Car.findByIdAndRemove(req.params.id)
        .then(() => {
            res.status(200).send("Car deleted");
        })
        .catch(err => {
            res.status(500).send(err);
        });
};
