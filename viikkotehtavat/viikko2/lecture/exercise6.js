// Create a program that joins two arrays together. If the same value appears in
// both arrays, the function should return that value only once. 

const arr1 = [1, 2, 3, 4];
const arr2 = [3, 4, 5, 6];

const letterArray = arr1.concat(arr2);

// removes dublicates from array
const arrDublicatesRemoved = letterArray.filter((num, index) => !letterArray.slice(index + 1).includes(num));

console.log(arrDublicatesRemoved);