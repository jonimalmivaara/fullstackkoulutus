function App() {

  const CurrentYear = () => {
    const year = new Date()
    return <h2>It is year {year.getFullYear()}</h2>
  }
  
  const ShowString = (props) => {
    return <h1>{props.hello}</h1>
  }

  return (
    <div >
      <ShowString hello="Hello React!"/>
      <CurrentYear />
    </div>
)}

export default App;
