// Find the first non-repeating character from a string.
// For example, in “aabbooooofffkkccjdddTTT” it would be “j”.


/* ******** FOR-LOOP RATKAISU ************ */

const string = "aabbooooofffkkccjdddTTT";

function firstUnique(string) {
    for(let i = 0; i < string.length; i++) {
        if(string[i] !== string[i + 1] && string[i] !== string[i - 1]) {
            return string[i];
        }
    }
}

console.log(firstUnique(string));

/* ******** MODERNI RATKAISU ************ */


// const letterArray = string.split("");

// const firstUniqueChar = letterArray.map(item => letterArray.indexOf(item) === letterArray.lastIndexOf(item) ? item : "").filter(item => !!item)[0];
// string.split("").find((s, i) => s !== string[i - 1] && s !== string[i + 1]);

// console.log(string);
