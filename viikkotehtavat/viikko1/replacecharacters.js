// Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.
// example: node .\replacecharacters.js g h "I have great grades for my grading" -> I have hreat hrades for my hrading

const sentence = process.argv[4];
const inputLetter = process.argv[2];
const outputLetter = process.argv[3];

if(!sentence || !inputLetter || !outputLetter) {
    console.log("Missing parameter!");
} else {
    replaceLetter();
}

function replaceLetter() {
    const wordsArray = sentence.split(" ");
    const newArray = [""];
    for(let i = 0; i < wordsArray.length; i++) {
        newArray.push(wordsArray[i].replace(inputLetter, outputLetter));
    }
    console.log(newArray.join(" "));
}