function aboveAverage(arr) {
    const arrayAvg = arr.reduce((acc, current) => {
        return acc + current / arr.length;
    },0);
    return arr.filter(num => num > arrayAvg);
}

console.log(aboveAverage([1, 5, 9, 3]));// outputs an array that has values greater than 4.5
