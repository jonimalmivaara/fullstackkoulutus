// level 1

let n = 4;
for(let i = 0; i <= n; i++) {
    let str = "";
    for(let j = 0; j < i; j++) {
        str += " &";
    }
    console.log(str);
}


// level 2

for (let i = 0; i < n; i++) {
    let str = "";
    for (let j =0; j < n - i; j++) {
        str += " ";
    }
    for (let k = 0; k <= i; k++) {
        str += "&";
    }
    console.log(str);  
} 


// for(let row = 0; row < n; row++) {
//     const ampersandAmount = 2 * row + 1;
//     let str = "";

//     for(let i = 1; i < n - row; i++) {
//         str += " ";
//     }

//     for(let i = 0; i < ampersandAmount; i++) {
//         str += "&";
//     }
//     console.log(str);
// }
