const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];

const dividedThree = arr.filter((num) => num % 3 === 0);
console.log(dividedThree);

const arrMultiple = arr.map((num) => num * 2);
console.log(arrMultiple);

const arrWithReduce = arr.reduce((acc, num) => acc + num);
console.log(arrWithReduce);