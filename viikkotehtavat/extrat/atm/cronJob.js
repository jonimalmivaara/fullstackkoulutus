import { CronJob } from "cron";
import * as fs from "fs";

function writeFile(file, data) {
    return new Promise(resolve => {
        fs.writeFile(file, JSON.stringify(data), (err) => {
            if(err) console.log(err);
            resolve();
        });
    });
}

async function logger(logs) {
    let timeStamp = Date.now();
    await writeFile(`./logs/log_${timeStamp}.json`, logs);
    await writeFile("log.json", []);
}

function cron() {
    new CronJob(
        "0 * * * * *", // every minute
        function() {
            let data; 
        
            try {
                data = fs.readFileSync("log.json", "utf-8");
            } catch (error) { 
                console.log(error);
            }
            const logs = JSON.parse(data); 
        
            logger(logs);
        },
        null,
        true,
    );
}

export default cron;
