// Create a countdown program using the setTimeout and
// callbacks functions.
// The program should output something like this.
// 3 ⇒ Wait 1 second
// ..2 ⇒ Wait another second
// ….1 ⇒ Wait the last 1 second..
// GO!

setTimeout(() => {
    console.log("3");
    setTimeout(() => {
        console.log("..2");
        setTimeout(() => {
            console.log("....1");
            setTimeout(() => {
                console.log("GO!");
            }, 1000);
        }, 1000);
    }, 1000);
}, 1000);