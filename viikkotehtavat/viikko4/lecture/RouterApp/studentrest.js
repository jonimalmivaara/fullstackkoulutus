import express from "express";
import { readJsonFile } from "./controllers/students.js";
import studentRouter from "./routes/students.js";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/students", studentRouter);

readJsonFile();
app.listen(5000, () => {
    console.log("Listening to 5000");
});