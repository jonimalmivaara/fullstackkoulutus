import axios from "axios";
import React, { useState, useEffect, useReducer } from "react";
import Category from "./ShowCategory";
import ProductModal from "./ProductModal";
import AddProductModal from "./AddProductModal";
import { reducer, initialState } from "./reducers/reducer";
import RenderProducts from "./RenderProducts";

export const ProductContext = React.createContext([]);

function Products() {
  const [products, dispatch] = useReducer(reducer, initialState);
  const [show, setShow] = useState(false);
  const [loadCategory, setLoadCategory] = useState("");
  const [item, setItem] = useState([]);

  const contextContainer = {
    products: products,
    setShow: setShow,
    item: item,
    setItem: setItem,
    show: show,
    dispatch: dispatch,
  };

  useEffect(() => {
    axios
      .get(`https://fakestoreapi.com/products/${loadCategory}`)
      .then((res) => {
        if (res.status === 200) {
          dispatch({
            type: "SET_PRODUCTS",
            payload: res.data,
          });
        } else {
          console.log("ERROR_LOADING_DATA");
        }
      });
  }, [loadCategory]);

  if (!products) {
    return <h3>Loading...</h3>;
  } else {
    return (
      <div>
        <ProductContext.Provider value={contextContainer}>
          <Category setLoadCategory={setLoadCategory} />
          <AddProductModal />
          <RenderProducts />
          <ProductModal />
        </ProductContext.Provider>
      </div>
    );
  }
}

export default Products;
