// Create a function (or multiple functions) that generates username and password from given firstname and lastname. Username: B + last 2 numbers from current year + 2 first letters from both last name and first name in lower case
// Password: 1 random letter + first letter of first name in lowercase + last letter of last name in uppercase + random special character + last 2 numbers fromcurrent year
// Example: John Doe -> B20dojo, mjE(20
// generateCredentials("John", "Doe")
// Get to know to ASCII table. Random letters and special characters can be searched from ASCII table with String.fromCharCode() method with indexes. Forexample String.fromCharCode(65) returns letter A.
// Hints: Generate random numbers (indexes) to get one random letter and one special character. Use range of 65 to 90 to get the LETTER and 33 to 47 toget the SPECIAL CHARACTER. Use build-in function to get the current year
// Output: “username: username, password: password”
// Links: http://www.asciitable.com/

function getRandom(min, max) { // gets random character from Strin.fromCharCode()
    min = Math.ceil(min);
    max = Math.floor(max) + 1; // + 1 to get maximum value given.
    return String.fromCharCode(Math.floor(Math.random() * (max - min) + min));
}

function generateCredentials(firstname, lastname) {
    const year = String((new Date()).getFullYear()).substring(2);
    const username =
        "B"+
        year+
        lastname.toLowerCase().substring(0,2)+
        firstname.toLowerCase().substring(0,2);
                        
    const password = 
        getRandom(65, 90)+ // random letter
        firstname.toLowerCase()[0]+
        lastname.toUpperCase().slice(-1)+
        getRandom(33, 47)+ // random character
        year;

    return(`username: ${username}, password: ${password}`);
}

console.log(generateCredentials("Jaska","Jokunen"));
