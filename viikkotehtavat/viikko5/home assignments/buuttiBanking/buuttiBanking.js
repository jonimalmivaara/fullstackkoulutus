import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import bankRouter from "./routes/bank.routes.js";

dotenv.config();
async function connectMongoose() {
    try {
        await mongoose.connect(
            "mongodb://localhost/buuttiBanking",
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true,
                // user: process.env.MONGO_USER,
                // pass: process.env.MONGO_PWD
            }
        );
    } catch (err) {
        console.log(err);
    }
}

export const app = express();
connectMongoose();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/bank", bankRouter);

app.listen(5000, () => {
    console.log("Listening to 5000");
});
