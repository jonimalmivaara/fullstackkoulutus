import mongoose from "mongoose";

const carSchema = new mongoose.Schema({
    car_make: String,
    car_model: String,
    car_modelyear: Number,
    price: Number,
});

const Car = mongoose.model("cars", carSchema);

export default Car;
