// Generates random number between min (inclusive) and max (inclusive)

function randomNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max) + 1;
    return Math.floor(Math.random() * (max - min) + min);
}

const nums = (randomNumber(2, 5));

console.log(nums);

// function randomInt(min, max) {
//     const range = max - min + 1;
//     return Math.floor(min + Math.random() * range);
// }

// console.log(randomInt(4, 9))