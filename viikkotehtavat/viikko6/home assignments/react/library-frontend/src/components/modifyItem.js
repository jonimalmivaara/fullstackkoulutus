import { useState } from "react";
import axios from "axios";
import { ModifyModal } from "./Modal";
import { BASE_URL } from "../App";

function ModifyItem(props) {
    const [isModifyModalOpen, setIsModifyModalOpen] = useState("modifyModal hidden");
    const [title, setTitle] = useState(props.item.name);
    const [author, setAuthor] = useState(props.item.author);
    const [image, setImage] = useState(props.item.img);

    const handleSubmit = (event) => {
      // prevent normal submit event
      event.preventDefault();

      console.log(props.owner.token);
      console.log(title, author, image);
      const data = {
        name: title,
        author: author,
        img: image
      };
  
        axios
          .put(BASE_URL + props.item.id, data, { headers: { Authorization: `bearer ${props.owner.token}`} })
          .then((res) => {
            const newItems = props.items.filter(item => item.id !== res.data.id);
            props.setItems(newItems)
            props.setLoading(true)
          });
    };

    return (
      <div>
        <ModifyModal setIsModifyModalOpen={setIsModifyModalOpen}/>
        <div className={isModifyModalOpen}>
          <div className="modifyForm">
            <img src={image} alt="img"/> 
            <form onSubmit={handleSubmit}>
              <input
                type="text"
                value={title}
                onChange={(event) => setTitle(event.target.value)}
                placeholder="Title"/>
              <input
                type="text"
                value={author}
                onChange={(event) => setAuthor(event.target.value)}
                placeholder="Author"/>  
              <input
                type="text"
                value={image}
                onChange={(event) => setImage(event.target.value)}
                placeholder="Image cover source"/><br/>
              <input type="submit" value="Change"/>
            </form>
          </div>
            <button className="btnModifyClose" onClick={() => setIsModifyModalOpen("modifyModal hidden")}>Back</button>
          </div>
      </div>
    )
  }

export default ModifyItem;