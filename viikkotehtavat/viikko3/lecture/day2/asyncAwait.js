function print(txt) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log(txt);
            resolve();
        }, 1000);
    });
}

async function countdown() {
    await print(3);
    await print(2);
    await print(1);
    await print("GO");
}

countdown();