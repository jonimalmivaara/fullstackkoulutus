import Student from "../models/student.model.js";

export const getAllStudents = (req, res) => {
    Student.find()
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.status(500).send(err);
        });
};

export const getStudent = async (req, res) => {
    try {
        const student = await Student.findById(req.params.id);
        if (student) res.json(student);
        else res.status(404).end();
    } catch(error) {
        console.log(error);
    }

};

export const postStudent = (req, res) => {
    const data = req.body;

    if(!data.firstname || !data.lastname) {
        res.status(400).send("Invalid data");
    } else {
        const student = new Student({
            firstname: data.firstname,
            lastname: data.lastname,
            birthYear: data.birthYear,
            email: data.email
        });

        student.save()
            .then(result => {
                console.log(result);
            })
            .catch(err => {
                console.log("Error", err);
            });

        res.status(200).send("Success");
    }
};

export const putStudent = async (req, res) => {
    try {
        const updatedStudent = await Student.findOneAndUpdate({
            id: req.params.id
        }, {
            firstname: req.body.firstname
        });
        res.send(updatedStudent);
    } catch(error) {
        res.status(500).send(error);
    }
};

export const deleteStudent = async (req, res) => {
    Student.findByIdAndRemove(req.params.id)
        .then(() => {
            res.status(200).send("Student deleted");
        })
        .catch(err => {
            res.status(500).send(err);
        });
};
