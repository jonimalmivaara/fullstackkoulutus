import express from "express";
import * as bookController from "../controllers/library.js";
import * as ownerController from "../controllers/owner.js";
import * as middlewareController from "../controllers/middleware.js";

const router = express.Router();

router.post("/register", ownerController.registerUserId);
router.post("/login", ownerController.postUserId);

router.use("/", middlewareController.getAuth);

router.get("/books", bookController.getAllBooks);
router.get("/users", bookController.getAllUsers);
router.get("/friends/:username", bookController.getAllFriends);
router.get("/books/:id", bookController.getBook);
router.get("/friendbooks", bookController.getAllFriendsBooks);


router.post("/books/", bookController.postBook);
router.post("/friends", bookController.postFriends);

router.put("/books/:id", bookController.putBook);

router.delete("/books/:id", bookController.deleteBook);

router.patch("/books/:id", bookController.patchRead);

router.use(middlewareController.unknownEndpoint);
router.use(middlewareController.errorHandler);

export default router;