"use strict";

/* eslint-disable no-unused-vars */

const modal = document.querySelector(".modal");
const loginDiv = document.querySelector(".loginDiv");
const showBooksDiv = document.querySelector(".showBooks");
const overlay = document.querySelector(".overlay");
const btnCloseModal = document.querySelector(".close-modal");
const btnsOpenModal = document.querySelector(".show-modal");
const showMenu = document.querySelector(".menu");

function openModal() {
    modal.classList.remove("hidden");
    console.log("moi");
    overlay.classList.remove("hidden");
}

function closeModal() {
    modal.classList.add("hidden");
    overlay.classList.add("hidden");
}

btnsOpenModal.addEventListener("click", openModal);

btnCloseModal.addEventListener("click", closeModal);
overlay.addEventListener("click", closeModal);

let token;

function handleLogin() {

    // eslint-disable-next-line no-undef
    axios.post("http://localhost:5000/login", {
        username: document.querySelector(".username").value,
        password: document.querySelector(".password").value
    })
        .then((response) => {
            console.log(response.data);
            token = response.data.token;
            loginDiv.classList.add("hidden");
            showBooksDiv.classList.remove("hidden");
            showMenu.classList.remove("hidden");
            renderBooks();
        })
        .catch((error) => {
            if (error.response.status === 401) {
                console.error(error.response.data.error);
            } else {
                console.error("Unknow error");
            }
        });
}

async function getBooks() {

    console.log(token);
    try {
        // eslint-disable-next-line no-undef
        const resp = await axios.get("http://localhost:5000/books", {
            headers: {
                Authorization: `bearer ${token}`,
            }
        });
        console.log(resp.data);
        return resp.data;
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
}

function deleteBook(id) {
    console.log(id);
    // eslint-disable-next-line no-undef
    axios.delete(`http://localhost:5000/books/${id}`, {
        headers: {
            Authorization: `bearer ${token}`,
        }
    });
    renderBooks();
}

function createBook() {
    const data = {
        name: document.querySelector(".name").value,
        author: document.querySelector(".author").value,
        img: document.querySelector(".img").value,
        read: true
    };
    // eslint-disable-next-line no-undef
    axios.post("http://localhost:5000/books", data, {
        headers: {
            Authorization: `bearer ${token}`,
        }
    });
    renderBooks();
}

async function renderBooks() {
    const books = await getBooks();
    document.querySelector(".showBooks").innerHTML = books.map(book => 
        `<div class="book" key={book.id} >
            <div class="bookInfo">
                <img src=${book.img} width=140></img>
                <h2>${book.name}</h2>
                <p>${book.author}</p>
                <p>${book.read ? "luettu": "ei luettu"}</p>
            </div>
                <button class="btnRemove" onclick="deleteBook('${book.id}')">X</button>
            </div>`);
}
