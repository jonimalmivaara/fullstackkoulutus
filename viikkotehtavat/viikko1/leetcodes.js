// Leetcodes from https://leetcode.com/problemset/all/

// 1. Two Sum

// const nums = [2,7,11,15];
// const target = 9;

// let twoSum = function(nums, target) {
//     for (let i = 0; i < nums.length; i++) {
//         for (let j = i + 1; j < nums.length; j++) {
//             if (nums[i] + nums[j] === target) {
//                 return [i, j];
//             }
//         }
//     }
// };

// console.log(twoSum(nums, target));

// 9. Palindrome Number

// let isPalindrome = function(x) {
//     let y = x.toString();
//     y = parseInt(y.split("").reverse().join(""));
//     console.log(y);
//     if(x === y) {
//         return true;
//     } else {
//         return false;
//     }
// };

// console.log(isPalindrome(-10));

// 13. Roman to Integer

// let romanToInt = function (string) {
//     const numbers = {
//         I: 1,
//         V: 5,
//         X: 10,
//         L: 50,
//         C: 100,
//         D: 500,
//         M: 1000,
//     };

//     // console.log(numerals["X"]);
//     const strLength = string.length;
//     let total = 0;

//     for(let i = 0; i < strLength; i++) {
//         console.log(numbers[string[i]]);
//         if(i < strLength -1 && numbers[string[i + 1]] > numbers[string[i]]) {
//             total -= numbers[string[i]];
//         } else {
//             total += numbers[string[i]];
//         }
//     }
//     return total;
// };
// console.log(romanToInt("XIII"));

// 14. Longest Common Prefix
