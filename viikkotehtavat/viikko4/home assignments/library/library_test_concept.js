import express from "express";
import { v4 as uuidv4 } from "uuid";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use(express.static("./public"));

const unknownEndpoint = (_req, res) => {
    res.status(404).send({ error: "No one here!"});
};

let books = [];

app.get("/books", (req, res) => {
    res.send(books);
    console.log("Get all");
});

app.get("/books/:id", (req, res) => {
    const id = req.params.id;
    const book = books.find(book => book.id === id);
    res.send(book);
    console.log("Get one");
});

app.post("/books/", (req, res) => {
    const book = req.body;
    book.id = uuidv4();
    books = books.concat(req.body);
    res.redirect("/");
    console.log("Post");
});

app.put("/books/:id", (req, res) => {
    const newBook = {...req.body, id: req.params.id};
    books = books.map(book => book.id ? newBook : book);
    res.redirect("/");
    console.log("Put");
});

app.delete("/books/:id", (req, res) => {
    const id = req.params.id;
    books = books.filter(book => book.id !== id);
    res.redirect("/");
    console.log("Delete");
});

app.use(unknownEndpoint);

app.listen(5000, () => {
    console.log("Listening port 5000");
});
