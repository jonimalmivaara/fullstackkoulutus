"use strict";

const colors = ["blue", "red", "black", "white", "brown", "yellow", "pink", "green"];

const button = document.querySelector(".btn");

function randomColor(colors) {
    return colors[Math.floor(Math.random() * colors.length)];
}

button.addEventListener("click", () => {
    const body = document.querySelector(".body");
    body.style["background-color"] = randomColor(colors);
});


// "use strict";

// const colorButton = document.getElementById("colorbutton");

// let currentIndex = 0;

// const colorlist = ["red", "green", "yellow", "white", "black"];

// colorButton.addEventListener("click", () => {
//     currentIndex += 1 + Math.floor(Math.random()*4);
//     const randomColor = colorlist[currentIndex % 5];
//     const body = document.querySelector("body");

//     body.style["background-color"] = randomColor;
// });