import express from "express";
import * as bankController from "../controllers/bank.controller.js";
import * as middlewareController from "../controllers/middleware.js";
import * as ownerController from "../controllers/owner.controller.js";

const router = express.Router();

// Login
router.post("/user", ownerController.postUser);
router.post("/login", ownerController.loginUser);

// Middleware
router.use("/", middlewareController.getAuth);

// BankControllers
router.get("/:user_id/balance", bankController.getUserBalance);
router.post("/transfer", bankController.postTransfer);
router.patch("/user/withdraw", bankController.patchWithdraw);
router.patch("/user/deposit", bankController.patchDeposit);
router.post("/accept_funds/:fund_id", bankController.postAcceptFund);

// OwnerControllers
router.patch("/update", ownerController.patchUsername); 
router.patch("/user/password", ownerController.patchPassword);
router.post("/request_funds", ownerController.postFundRequest);
router.get("/requests/:user_id", ownerController.getRequests);
router.delete("/requests/:fund_id", ownerController.deleteRequest);

export default router;