/* Create a very basic command line application for logging in.
First generate a correct password hash and store it into a
“correctPassword” variable.
Application should ask a user for a password and compare
it to the generated correct password hash. If the passwords
match, print out “Logged in” or “Wrong password”.
 */

/* TO CREATE HASHED PASSWORD

const saltRounds = 10;
const password = "secretpassword";

const passwordHash = await bcrypt.hash(password, saltRounds);  */

import bcrypt from "bcrypt";
import dotenv from "dotenv";

dotenv.config();

const passwordInput = process.argv[2];
const hash = process.env.SECRET_HASH;

async function checkPassword() {

    const match = await bcrypt.compare(passwordInput, hash);

    return (match ? console.log("Logged in") : console.log("Wrong password"));
}

checkPassword();
