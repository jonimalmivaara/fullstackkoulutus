const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

// Level 1

let max = 0;
for(let i = 0; i < arr.length; i++) {
    if(max < arr[i]) {
        max = arr[i];
    }
}

console.log(max);

// Level 2

let max1 = 0;
let secondLargest = 0;
for(let i = 0; i < arr.length; i++) {
    if(max1 < arr[i]) {
        max1 = arr[i];
    } else if (secondLargest < arr[i]) {
        secondLargest = arr[i];
    }
}

console.log(secondLargest);
