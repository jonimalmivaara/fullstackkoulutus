// Create a function that takes in an operator and two numbers and returns the result.
// For example, if the operator is "+", sum the given numbers. In addition to sum, calculator can also calculate differences, multiplications and divisions.
// If theoperator is something else, return some error message like "Can't do that!"

function calculator(operator, num1, num2) {
    switch (operator) {
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "*":
            return num1 * num2;
        case "/":
            return num1 * num2;
        default:
            return "Can't do that!";
    }
}

console.log(calculator("+", 2, 3));
console.log(calculator("-", 2, 3));
console.log(calculator("*", 2, 3));
console.log(calculator("/", 2, 3));
console.log(calculator("ops", 2, 3));

