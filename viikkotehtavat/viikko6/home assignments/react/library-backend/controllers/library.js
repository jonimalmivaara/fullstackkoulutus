import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import dotenv from "dotenv";
import { currentUser, users} from "./owner.js";

dotenv.config();

let books = [];

function updateJsonFile(file, itemsToUpdate) {
    fs.writeFileSync(file, JSON.stringify(itemsToUpdate), "utf-8");
}

export function readJsonFile() {
    try {
        books = JSON.parse(fs.readFileSync("books.json", "utf-8"));
    } catch {
        books = [];
    }
}

export const getAllBooks = (req, res) => {
    const BooksById = books.filter(({owner_id}) => owner_id === currentUser);
    res.send(BooksById);
};

export const getAllUsers = (req, res) => {
    res.send(users);
};

export const getBook = (req, res) => {
    const id = req.params.id;
    const book = books.find(book => book.id === id);
    res.send(book);
};

export const getAllFriends = (req, res) => {
    const username = req.params.username;
    const userIndex = findUserByIndex(username); 
    res.send(JSON.stringify(users[userIndex]["friends"]));
};

export const getAllFriendsBooks = (req, res) => { // TÄHÄN JÄIT!!!
    console.log(currentUser);
    console.log(user);
 
};

export const postBook = (req, res) => {
    const book = req.body;
    book.id = uuidv4();
    book.owner_id = currentUser;
    books = books.concat(req.body);
    updateJsonFile("books.json", books);
    res.sendStatus(200);
};

function findUserByIndex(username) {
    return users.findIndex(user => user.username === username);
}

export const postFriends = (req, res) => {
    const username = req.body.username;
    const friendname = req.body.friendname;
    const userIndex = findUserByIndex(username);
    users[userIndex]["friends"].push(friendname);
    updateJsonFile("users.json", users);
    res.sendStatus(200);
};

export const putBook = (req, res) => {
    const id = req.params.id;
    const body = req.body;
    const newBook = {name: body.name, author: body.author, img: body.img, id: id, owner_id: currentUser};
    console.log(newBook);
    books = books.map(book => book.id === id ? newBook : book);
    updateJsonFile("books.json", books);
    res.send("Modified");
};

export const deleteBook = (req, res) => {
    const id = req.params.id;
    books = books.filter(book => book.id !== id);
    updateJsonFile("books.json", books);
    res.send("Deleted");
};

export const patchRead = (req, res) => {
    const id = req.params.id;
    const read = req.body.read;
    books.map(book => {
        if (book.id === id) {
            book.read = read;
        }
    });
    updateJsonFile("books.json", books);
    res.send(books);
};