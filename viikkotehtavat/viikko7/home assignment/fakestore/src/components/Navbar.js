function Navbar() {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container px-4 px-lg-5">
          <a className="navbar-brand" href="#!">
            FakeShop
          </a>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
