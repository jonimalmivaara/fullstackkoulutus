const numbers = [];

while(numbers.length < 7) {
    const lottoNumber = Math.floor(Math.random() * 40) + 1;

    if(!numbers.includes(lottoNumber)) {
        numbers.push(lottoNumber);
    }
}
numbers.sort((a, b) => a - b);
console.log(numbers);
let i = 0;
const timer = setInterval(() => {
    if(i === 6) {
        clearInterval(timer);
    }
    console.log(numbers[i]);
    i++;
}, 1000); 