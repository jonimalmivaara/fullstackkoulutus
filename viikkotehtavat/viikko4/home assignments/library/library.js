import express from "express";
import { readJsonFile } from "./controllers/library.js";
import libraryRouter from "./routes/library.js";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use(express.static("./public"));
app.use("/", libraryRouter);

readJsonFile();

app.listen(5000, () => {
    console.log("Listening port 5000");
});
