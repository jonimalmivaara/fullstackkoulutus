import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

async function connectMongoose() { 
    try {
        await mongoose.connect(
            "mongodb://localhost/studentDB",
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true,
                user: process.env.MONGO_USER,
                pass: process.env.MONGO_PWD
            }
        );
    } catch (err){
        console.log(err);
    }
}

const StudentSchema = new mongoose.Schema({
    firstname: String,
    lastname: String,
    birthYear: Number,
    email: String
});


connectMongoose(); // Connect first before adding data!!

const StudentsCollection = mongoose.model("students", StudentSchema);

StudentsCollection.find()
    .then(result => result.forEach(console.log));

const result = await StudentsCollection.findOne({name: "Joni"}); // Etsi yksi
console.log(result);

/* const Student = new StudentsCollection({
    firstname: "Mika",
    lastname: "Jokunen",
    birthYear: 1982,
    email: "mj@gmail.com"
});

Student.save()
    .then(result => {
        console.log(result);
        mongoose.connection.close();
    })
    .catch(err => {
        console.log("Error", err);
        mongoose.connection.close();
    }); */



