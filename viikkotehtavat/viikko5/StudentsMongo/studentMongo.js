import express from "express";
import studentRouter from "./routes/students.router.js";
import dotenv from "dotenv";
import mongoose from "mongoose";

dotenv.config();

async function connectMongoose() { 
    try {
        await mongoose.connect(
            "mongodb://localhost/studentDB",
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true,
                user: process.env.MONGO_USER,
                pass: process.env.MONGO_PWD
            }
        );
    } catch (err){
        console.log(err);
    }
}

connectMongoose();

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/students", studentRouter);

app.listen(5000, () => {
    console.log("Listening to 5000");
});