// Create a program that every time you run it, prints out an array with differently randomized order of the array above.

const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

function randomArray() {
    const random = array.map((value) => ({ value, order : Math.random() }));
    const sorted = random.sort((a , b) => a.order - b.order);
    const randomized = sorted.map((num) => num.value);
    return randomized;
}

console.log(randomArray(array));