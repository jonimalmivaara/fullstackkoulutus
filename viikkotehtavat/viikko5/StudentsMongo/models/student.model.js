import mongoose from "mongoose";

const StudentSchema = new mongoose.Schema({
    firstname: String,
    lastname: String,
    birthYear: Number,
    email: String
});

const Student = mongoose.model("students", StudentSchema);

export default Student;