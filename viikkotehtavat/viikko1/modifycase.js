const upOrLowCase = process.argv[2];
const sentence = process.argv[3];

if(!upOrLowCase || !sentence) {
    console.log("Give first parameter to lower/upper and second with quotation marks");
} else if(upOrLowCase === "upper") {
    console.log(sentence.toLocaleUpperCase());
} else if(upOrLowCase === "lower") {
    console.log(sentence.toLocaleLowerCase());
} else {
    console.log("First prameter is not lower or upper");
}