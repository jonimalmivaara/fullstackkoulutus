const voters = [
    {name:"Bob" , age: 30, voted: true},
    {name:"Jake" , age: 32, voted: true},
    {name:"Kate" , age: 25, voted: false},
    {name:"Sam" , age: 20, voted: false},
    {name:"Phil" , age: 21, voted: true},
    {name:"Ed" , age:55, voted:true},
    {name:"Tami" , age: 54, voted:true},
    {name: "Mary", age: 31, voted: false},
    {name: "Becky", age: 43, voted: false},
    {name: "Joey", age: 41, voted: true},
    {name: "Jeff", age: 30, voted: true},
    {name: "Zack", age: 19, voted: false}
];

function voterResults(arr) {
    const votersAgeResults = { 
        numYoungVotes: 0,
        numYoungPeople: 0,
        numMidVotesPeople: 0,
        numMidsPeople: 0,
        numOldVotesPeople: 0,
        numOldsPeople: 0 
    };

    return arr.reduce( (acc, voter ) => {
        let voting = voter.voted? 1 : 0;
        if(voter.age <= 25) {
            acc.numYoungVotes += voting;
            acc.numYoungPeople++;
        } else if(voter.age <= 35) {
            acc.numMidVotesPeople += voting;
            acc.numMidsPeople++;
        } else {
            acc.numOldVotesPeople += voting;
            acc.numOldsPeople++;
        }
        return acc; 
        
    },votersAgeResults);
}


console.log(voterResults(voters)); // Returned value shown below:
/*
{ numYoungVotes: 1,
  numYoungPeople: 4,
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numOldVotesPeople: 3,
  numOldsPeople: 4 
}
*/