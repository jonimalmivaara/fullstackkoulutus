for(let i = 0; i <= 1000; i += 100) { // 0 100 200 300 400 500 600 700 800 900 1000
    console.log(i);
}

for(let i = 1; i <= 128; i *= 2) { // 1 2 4 8 16 32 64 128
    console.log(i);
}

for(let i = 3; i <= 15; i += 3) { // 3 6 9 12 15
    console.log(i);
}

for(let i = 9; i >= 0; i--) { // 9 8 7 6 5 4 3 2 1 0
    console.log(i);
}

for(let i = 1; i < 4; i++) { // 1 1 1 2 2 2 3 3 3 4 4 4
    console.log(i);
    console.log(i);
    console.log(i);
}

for(let i = 1; i < 5; i += 0.33) { // 1 1 1 2 2 2 3 3 3 4 4 4
    console.log(Math.floor(i));
}

let num = 0;
for(let i = 0; i < 12; i++) { // 1 1 1 2 2 2 3 3 3 4 4 4
    if(i % 3 === 0) {
        num++;
    }
    console.log(num);
}

for(let i = 1; i < 5; i++) { // 1 1 1 2 2 2 3 3 3 4 4 4
    for(let j = 0; j < 3; j++) {
        console.log(i);
    }
}

for(let i = 0; i < 4; i++) { // 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4
    for(let j = 0; i <= 4; j++) {
        console.log(j);
    }
}