// Create a "like" function that takes in a array of names. Depending on a number of names (or length of the array) the function must return strings as follows:
// For 4 or more names, "2 others" simply increases.

function likes(arr) {
    switch (arr.length) {
        case 0:
            return "no one likes this";
        case 1:
            return `${arr[0]} likes this`;
        case 2:
            return `${arr[0]} and ${arr[1]} likes this`;
        case 3:
            return `${arr[0]}, ${arr[1]} and ${arr[2]} likes this`;
        case 4:
        default:
            return `${arr[0]}, ${arr[1]} and ${arr.length - 2} others`;
    }
}

console.log(likes([])); // "no one likes this"
console.log(likes(["John"])); // "John likes this"
console.log(likes(["Mary", "Alex"])); // "Mary and Alex like this"
console.log(likes(["John", "James", "Linda"])); // "John, James and Linda like this"
console.log(likes(["Alex", "Linda", "Mark", "Max"])); // must be "Alex, Linda and 2 others 