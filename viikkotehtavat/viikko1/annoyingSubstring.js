const sentence = process.argv[2];

if(!sentence) {
    console.log("Missing parameter! example: node annoyingSubstring.js 'Hey I'm alive!' -> Hey I'm");
} else {
    const wordsArray = sentence.split(" ");
    wordsArray.splice(-1);
    console.log(wordsArray.join(" "));
}
