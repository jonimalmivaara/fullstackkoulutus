import React, { useState, useEffect } from "react";
import axios from "axios";
import CreateItem from './CreateItem';
import ModifyItem from './ModifyItem';
import DeleteItem from './DeleteItem';
import {
  useNavigate
} from "react-router-dom";
import { OwnerContext } from "./Provider";
import { BASE_URL } from "../App";
import ChangeRead from "./ChangeRead";

function Library() {
    const [items, setItems] = useState([]);
    const [loading, setLoading] = useState(true);
    const { owner } = React.useContext(OwnerContext);
  
    useEffect(() => {
      if (owner.auth) {
        axios
          .get(BASE_URL, { headers: { Authorization: `bearer ${owner.token}` } })
          .then((res) => {
            setItems(res.data);
            setLoading(false);
          });
        } else {
          navigate("/");
        }
    }, [loading])
 
    const RenderItems = () => {
        if (items.length === 0) {
            return(
              <div className="loading" style={{marginRight:"10em"}}>
                <p>Loading, please wait...</p>
              </div>
            )
        
          } else {
            return (
                <div>
                {items.map((item) => 
                    <div key={item.id} className="Item">
                    <DeleteItem items={items} setItems={setItems} id={item.id} setLoading={setLoading} owner={owner}/>
                    <img src={item.img} alt="img"></img>
                    <h3 className="ItemTitle">{item.name}</h3>
                    <p className="ItemText">{item.author}</p>
                    <ChangeRead  setItems={setItems} items={items} setLoading={setLoading} item={item} owner={owner}/>
                    <ModifyItem setItems={setItems} items={items} setLoading={setLoading} item={item} owner={owner}/>
                </div>
                )}
                </div>
            )
        }
    }
  
    const navigate = useNavigate();
  
    const handleLogout = () => {
      navigate("/");
    }
  
    return(
    <div >
        <div className="buttons-div">
            <input className="btnLogout" value='Logout' onClick={() => handleLogout()}/>
            <CreateItem setItems={setItems} items={items} setLoading={setLoading} owner={owner}/>
            <p className="user">{owner.name}</p>
        </div>
            <RenderItems />
    </div>
    )
}
  

export default Library