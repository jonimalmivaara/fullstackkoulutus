import React, { useState } from "react";
import { OwnerContext } from "./Provider";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function Login() { 
    const [error, setError] = useState('');
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const { login } = React.useContext(OwnerContext);
  
    const onChangeHandler = (event) => {
      const {name, value} = event.currentTarget;
      if (name === 'name') setName(value);
      else if (name === 'password') setPassword(value);
    }
  
    const navigate = useNavigate()
    
    const handleLogin = () => {
      axios.post('http://localhost:5000/login', { 'username':name, 'password':password })
        .then(response => {
          login(name, response.data.token);
          navigate('/home');
        })
        .catch(error => {
          if (error.response.status === 401) {
            setError(error.response.data.error);
          } else {
            setError("Unknow error");
          }
        })
    }
  
    return (
      <div className="login-page">
        <form>
          <input 
            type='text' 
            placeholder="Username" 
            name="name" 
            onChange={(event) => onChangeHandler(event)}/>
          <input  
            type='password' 
            placeholder="Password" 
            name="password" 
            onChange={(event) => onChangeHandler(event)}/>
          <input type='button' value='Login' onClick={() => handleLogin()}/>
        </form>
        <span style={{fontSize:"smaller", cursor:"pointer"}} onClick={() => navigate("./register")}>Not user yet? Click here to register.</span>
      </div>
    )
  }

export default Login;