const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
});

const balance = 100;
const isActive = true;
let checkBalance = false;

function app() {
    readline.question("Check your balance (yes/no)? ", (answer) => {
        if (answer  === "yes") {
            checkBalance = true;
            atm();
        } else {
            console.log("Have a nice day!");
            process.exit(0);
        }
    });
}

function atm() {
    if(checkBalance && isActive && balance > 0) {
        console.log(`Your balance is ${balance}`);
    } else if(!isActive) {
        console.log("Your account is not active");
    } else if(balance === 0) {
        console.log("Your account is empty");
    } else if(balance < 0) {
        console.log(`Your balance is negative (${balance})`);
    }
    process.exit(0);
}

app();